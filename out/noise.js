"use strict";
var feng3d;
(function (feng3d) {
    /**
     * 柏林噪音
     *
     * @see http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
     * @see https://mrl.nyu.edu/~perlin/noise/
     *
     */
    var Noise = /** @class */ (function () {
        /**
         * 构建柏林噪音
         *
         * @param seed 随机种子
         */
        function Noise(seed) {
            if (seed === void 0) { seed = 0; }
            this._seed = 0;
            this._p = [];
            this.seed = seed;
        }
        /**
         * 1D 经典噪音
         *
         * @param x X轴数值
         */
        Noise.prototype.perlin1 = function (x) {
            var perm = this._p;
            // Find unit grid cell containing point
            var X = Math.floor(x);
            // Get relative xyz coordinates of point within that cell
            x = x - X;
            // Wrap the integer cells at 255 (smaller integer period can be introduced here)
            X = X & 255;
            // Calculate a set of eight hashed gradient indices
            var gi00 = perm[X] % 2;
            var gi10 = perm[X + 1] % 2;
            // Calculate noise contributions from each of the eight corners
            var n00 = dot1(grad1[gi00], x);
            var n10 = dot1(grad1[gi10], x - 1);
            // Compute the fade curve value for each of x, y
            var u = fade(x);
            // Interpolate along x the contributions from each of the corners
            var nx0 = mix(n00, n10, u);
            return nx0;
        };
        /**
         * 2D 经典噪音
         *
         * @param x X轴数值
         * @param y Y轴数值
         */
        Noise.prototype.perlin2 = function (x, y) {
            var perm = this._p;
            // Find unit grid cell containing point
            var X = Math.floor(x);
            var Y = Math.floor(y);
            // Get relative xyz coordinates of point within that cell
            x = x - X;
            y = y - Y;
            // Wrap the integer cells at 255 (smaller integer period can be introduced here)
            X = X & 255;
            Y = Y & 255;
            // Calculate a set of eight hashed gradient indices
            var gi00 = perm[X + perm[Y]] % 4;
            var gi10 = perm[X + 1 + perm[Y]] % 4;
            var gi01 = perm[X + perm[Y + 1]] % 4;
            var gi11 = perm[X + 1 + perm[Y + 1]] % 4;
            // Calculate noise contributions from each of the eight corners
            var n00 = dot2(grad2[gi00], x, y);
            var n10 = dot2(grad2[gi10], x - 1, y);
            var n01 = dot2(grad2[gi01], x, y - 1);
            var n11 = dot2(grad2[gi11], x - 1, y - 1);
            // Compute the fade curve value for each of x, y
            var u = fade(x);
            var v = fade(y);
            // Interpolate along x the contributions from each of the corners
            var nx0 = mix(n00, n10, u);
            var nx1 = mix(n01, n11, u);
            // Interpolate the four results along y
            var nxy = mix(nx0, nx1, v);
            return nxy;
        };
        /**
         * 3D 经典噪音
         *
         * @param x X轴数值
         * @param y Y轴数值
         * @param z Z轴数值
         */
        Noise.prototype.perlin3 = function (x, y, z) {
            var perm = this._p;
            // Find unit grid cell containing point
            var X = Math.floor(x);
            var Y = Math.floor(y);
            var Z = Math.floor(z);
            // Get relative xyz coordinates of point within that cell
            x = x - X;
            y = y - Y;
            z = z - Z;
            // Wrap the integer cells at 255 (smaller integer period can be introduced here)
            X = X & 255;
            Y = Y & 255;
            Z = Z & 255;
            // Calculate a set of eight hashed gradient indices
            var gi000 = perm[X + perm[Y + perm[Z]]] % 12;
            var gi100 = perm[X + 1 + perm[Y + perm[Z]]] % 12;
            var gi010 = perm[X + perm[Y + 1 + perm[Z]]] % 12;
            var gi110 = perm[X + 1 + perm[Y + 1 + perm[Z]]] % 12;
            var gi001 = perm[X + perm[Y + perm[Z + 1]]] % 12;
            var gi101 = perm[X + 1 + perm[Y + perm[Z + 1]]] % 12;
            var gi011 = perm[X + perm[Y + 1 + perm[Z + 1]]] % 12;
            var gi111 = perm[X + 1 + perm[Y + 1 + perm[Z + 1]]] % 12;
            // Calculate noise contributions from each of the eight corners
            var n000 = dot(grad3[gi000], x, y, z);
            var n100 = dot(grad3[gi100], x - 1, y, z);
            var n010 = dot(grad3[gi010], x, y - 1, z);
            var n110 = dot(grad3[gi110], x - 1, y - 1, z);
            var n001 = dot(grad3[gi001], x, y, z - 1);
            var n101 = dot(grad3[gi101], x - 1, y, z - 1);
            var n011 = dot(grad3[gi011], x, y - 1, z - 1);
            var n111 = dot(grad3[gi111], x - 1, y - 1, z - 1);
            // Compute the fade curve value for each of x, y, z
            var u = fade(x);
            var v = fade(y);
            var w = fade(z);
            // Interpolate along x the contributions from each of the corners
            var nx00 = mix(n000, n100, u);
            var nx01 = mix(n001, n101, u);
            var nx10 = mix(n010, n110, u);
            var nx11 = mix(n011, n111, u);
            // Interpolate the four results along y
            var nxy0 = mix(nx00, nx10, v);
            var nxy1 = mix(nx01, nx11, v);
            // Interpolate the two last results along z
            var nxyz = mix(nxy0, nxy1, w);
            return nxyz;
        };
        /**
         * N阶经典噪音
         *
         * 如果是1D，2D，3D噪音，最好选用对于函数，perlinN中存在for循环因此效率比perlin3等性能差3到5（8）倍！
         *
         * 满足以下运算
         * perlinN(x) == perlin1(x)
         * perlinN(x,y) == perlin2(x,y)
         * perlinN(x,y,z) == perlin3(x,y,z)
         *
         * @param ps 每个轴的数值
         */
        Noise.prototype.perlinN = function () {
            var ps = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                ps[_i] = arguments[_i];
            }
            var perm = this._p;
            var n = ps.length;
            // 在格子内对应每个轴的位置
            var pp = [];
            // 所属格子对应每个轴的索引
            var PS = [];
            // 在格子内对应每个轴的混合权重
            var PF = [];
            for (var i = 0; i < n; i++) {
                var p = ps[i];
                // 找到所属单元格
                var P = Math.floor(p);
                // 获取所在单元格内的位置
                p = p - P;
                // 单元格以255为周期
                P = P & 255;
                //
                pp[i] = p;
                PS[i] = P;
                // 分别计算每个轴的混合度
                PF[i] = fade(p);
            }
            //
            var gradN = createGrad(n);
            // 边的数量
            var numEdge = gradN.length;
            // if (n > 1)
            // {
            //     console.assert(numEdge == Math.pow(2, n - 1) * n, `边的数量不对！`)
            // }
            //
            var bits = getBits(n);
            var dns = [];
            //
            for (var i = 0, len = bits.length; i < len; i++) {
                var bit = bits[i];
                var bitn = bit.length;
                // 计算索引
                var gi = 0;
                while (bitn > 0) {
                    bitn--;
                    gi = perm[PS[bitn] + bit[bitn] + gi];
                    // if (isNaN(gi))
                    //     debugger;
                }
                // 获取 grad
                var grad = gradN[gi % numEdge];
                bitn = bit.length;
                // 计算点乘 dot运算
                var dn = 0;
                while (bitn > 0) {
                    bitn--;
                    dn += grad[bitn] * (pp[bitn] - bit[bitn]);
                }
                dns[i] = dn;
            }
            // 进行插值
            for (var i = 0; i < n; i++) {
                // 每次前后两个插值
                for (var j = 0, len = dns.length; j < len; j += 2) {
                    dns[j / 2] = mix(dns[j], dns[j + 1], PF[i]);
                }
                // 每波插值后 长度减半
                dns.length = dns.length >> 1;
            }
            // console.assert(dns.length == 1, `结果长度不对！`)
            return dns[0];
        };
        Object.defineProperty(Noise.prototype, "seed", {
            /**
             * This isn't a very good seeding function, but it works ok. It supports 2^16
             * different seed values. Write something better if you need more seeds.
             */
            get: function () {
                return this._seed;
            },
            set: function (v) {
                this._seed = v;
                var p = this._p;
                if (v > 0 && v < 1) {
                    // Scale the seed out
                    v *= 65536;
                }
                v = Math.floor(v);
                if (v < 256) {
                    v |= v << 8;
                }
                for (var i = 0; i < 256; i++) {
                    var v0;
                    if (i & 1) {
                        v0 = permutation[i] ^ (v & 255);
                    }
                    else {
                        v0 = permutation[i] ^ ((v >> 8) & 255);
                    }
                    p[i] = p[i + 256] = v0;
                }
            },
            enumerable: true,
            configurable: true
        });
        return Noise;
    }());
    feng3d.Noise = Noise;
    /**
     *
     * @param n
     *
     * len = 2^(n-1) * n
     */
    function createGrad(n) {
        if (createGradCache[n])
            return createGradCache[n];
        var gradBase = createGradBase(n - 1);
        var grad = [];
        if (n > 1) {
            for (var i = n - 1; i >= 0; i--) {
                for (var j = 0; j < gradBase.length; j++) {
                    var item = gradBase[j].concat();
                    item.splice(i, 0, 0);
                    grad.push(item);
                }
            }
        }
        else {
            grad = gradBase;
        }
        createGradCache[n] = grad;
        return grad;
    }
    feng3d.createGrad = createGrad;
    var createGradCache = {};
    function createGradBase(n) {
        if (n < 2)
            return [
                [1], [-1],
            ];
        var grad = createGradBase(n - 1);
        for (var i = 0, len = grad.length; i < len; i++) {
            var item = grad[i];
            grad[i] = item.concat(1);
            grad[i + len] = item.concat(-1);
        }
        return grad;
    }
    function getBits(n) {
        if (getBitsChace[n])
            return getBitsChace[n];
        if (n < 2)
            return [
                [0], [1],
            ];
        var grad = getBits(n - 1);
        for (var i = 0, len = grad.length; i < len; i++) {
            var item = grad[i];
            grad[i] = item.concat(0);
            grad[i + len] = item.concat(1);
        }
        getBitsChace[n] = grad;
        return grad;
    }
    feng3d.getBits = getBits;
    var getBitsChace = {};
    var grad1 = [
        [1], [-1],
    ];
    var grad2 = [
        [1, 0], [-1, 0],
        [0, 1], [0, -1],
    ];
    var grad3 = [
        [1, 1, 0], [-1, 1, 0], [1, -1, 0], [-1, -1, 0],
        [1, 0, 1], [-1, 0, 1], [1, 0, -1], [-1, 0, -1],
        [0, 1, 1], [0, -1, 1], [0, 1, -1], [0, -1, -1]
    ];
    var permutation = [
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    ];
    function dot(g, x, y, z) {
        return g[0] * x + g[1] * y + g[2] * z;
    }
    function dot2(g, x, y) {
        return g[0] * x + g[1] * y;
    }
    function dot1(g, x) {
        return g[0] * x;
    }
    function mix(a, b, t) {
        return (1 - t) * a + t * b;
    }
    function fade(t) {
        return t * t * t * (t * (t * 6 - 15) + 10);
    }
})(feng3d || (feng3d = {}));
var feng3d;
(function (feng3d) {
    var PerlinNoise = /** @class */ (function () {
        function PerlinNoise(seed) {
            if (seed === void 0) { seed = 0; }
            this._seed = 0;
            this._p = [];
            this.seed = seed;
        }
        /**
         *
         * @param x
         */
        PerlinNoise.prototype.perlin1 = function (x) {
            var p = this._p;
            x = Math.abs(x);
            var floorX = Math.floor(x);
            var X = floorX & 255; // FIND UNIT CUBE THAT
            x -= floorX; // FIND RELATIVE X,Y,Z
            var u = fade(Math.min(x, 1.0)); // COMPUTE FADE CURVES
            var A = p[X], // HASH COORDINATES OF
            B = p[X + 1]; // THE 8 CUBE CORNERS,
            var res = lerp(u, grad1(p[A], x), // AND ADD
            grad1(p[B], x - 1)); // FROM  8
            return res;
        };
        /**
         * Unity中曾使用该算法
         *
         * @param x
         * @param y
         */
        PerlinNoise.prototype.perlin2 = function (x, y) {
            var p = this._p;
            x = Math.abs(x);
            y = Math.abs(y);
            var floorX = Math.floor(x);
            var floorY = Math.floor(y);
            var X = floorX & 255; // FIND UNIT CUBE THAT
            var Y = floorY & 255; // CONTAINS POINT.
            x -= floorX; // FIND RELATIVE X,Y,Z
            y -= floorY; // OF POINT IN CUBE.
            var u = fade(Math.min(x, 1.0)); // COMPUTE FADE CURVES
            var v = fade(Math.min(y, 1.0)); // FOR EACH OF X,Y,Z.
            var A = p[X] + Y, AA = p[A], AB = p[A + 1], // HASH COORDINATES OF
            B = p[X + 1] + Y, BA = p[B], BB = p[B + 1]; // THE 8 CUBE CORNERS,
            var res = lerp(v, lerp(u, grad2(p[AA], x, y), // AND ADD
            grad2(p[BA], x - 1, y)), // BLENDED
            lerp(u, grad2(p[AB], x, y - 1), // RESULTS
            grad2(p[BB], x - 1, y - 1))); // FROM  8
            return res;
        };
        /**
         *
         *
         * This code implements the algorithm I describe in a corresponding SIGGRAPH 2002 paper.
         *
         * JAVA REFERENCE IMPLEMENTATION OF IMPROVED NOISE - COPYRIGHT 2002 KEN PERLIN.
         *
         * @see https://mrl.nyu.edu/~perlin/noise/
         */
        PerlinNoise.prototype.perlin3 = function (x, y, z) {
            var p = this._p;
            var X = Math.floor(x) & 255, // FIND UNIT CUBE THAT
            Y = Math.floor(y) & 255, // CONTAINS POINT.
            Z = Math.floor(z) & 255;
            x -= Math.floor(x); // FIND RELATIVE X,Y,Z
            y -= Math.floor(y); // OF POINT IN CUBE.
            z -= Math.floor(z);
            var u = fade(x), // COMPUTE FADE CURVES
            v = fade(y), // FOR EACH OF X,Y,Z.
            w = fade(z);
            var A = p[X] + Y, AA = p[A] + Z, AB = p[A + 1] + Z, // HASH COORDINATES OF
            B = p[X + 1] + Y, BA = p[B] + Z, BB = p[B + 1] + Z; // THE 8 CUBE CORNERS,
            return lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z), // AND ADD
            grad(p[BA], x - 1, y, z)), // BLENDED
            lerp(u, grad(p[AB], x, y - 1, z), // RESULTS
            grad(p[BB], x - 1, y - 1, z))), // FROM  8
            lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1), // CORNERS
            grad(p[BA + 1], x - 1, y, z - 1)), // OF CUBE
            lerp(u, grad(p[AB + 1], x, y - 1, z - 1), grad(p[BB + 1], x - 1, y - 1, z - 1))));
        };
        Object.defineProperty(PerlinNoise.prototype, "seed", {
            /**
             * This isn't a very good seeding function, but it works ok. It supports 2^16
             * different seed values. Write something better if you need more seeds.
             */
            get: function () {
                return this._seed;
            },
            set: function (v) {
                this._seed = v;
                var p = this._p;
                if (v > 0 && v < 1) {
                    // Scale the seed out
                    v *= 65536;
                }
                v = Math.floor(v);
                if (v < 256) {
                    v |= v << 8;
                }
                for (var i = 0; i < 256; i++) {
                    var v0;
                    if (i & 1) {
                        v0 = permutation[i] ^ (v & 255);
                    }
                    else {
                        v0 = permutation[i] ^ ((v >> 8) & 255);
                    }
                    p[i] = p[i + 256] = v0;
                }
            },
            enumerable: true,
            configurable: true
        });
        return PerlinNoise;
    }());
    feng3d.PerlinNoise = PerlinNoise;
    var permutation = [
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    ];
    function fade(t) { return t * t * t * (t * (t * 6 - 15) + 10); }
    function lerp(t, a, b) { return a + t * (b - a); }
    /**
     * 没看懂？
     *
     * @param hash
     * @param x
     * @param y
     * @param z
     */
    function grad(hash, x, y, z) {
        var h = hash & 15; // CONVERT LO 4 BITS OF HASH CODE
        var u = h < 8 ? x : y, // INTO 12 GRADIENT DIRECTIONS.
        v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
    /**
     * unity中使用？
     *
     * @param hash
     * @param x
     * @param y
     */
    function grad2(hash, x, y) {
        var z = 0;
        var h = hash & 15; // CONVERT LO 4 BITS OF HASH CODE
        var u = h < 8 ? x : y, // INTO 12 GRADIENT DIRECTIONS.
        v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
    /**
     * 这么写？
     *
     * @param hash
     * @param x
     */
    function grad1(hash, x) {
        var y = 0, z = 0;
        var h = hash & 15; // CONVERT LO 4 BITS OF HASH CODE
        var u = h < 8 ? x : y, // INTO 12 GRADIENT DIRECTIONS.
        v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
})(feng3d || (feng3d = {}));
var feng3d;
(function (feng3d) {
    /**
     * A speed-improved perlin and simplex noise algorithms for 2D.
     *
     * Based on example code by Stefan Gustavson (stegu@itn.liu.se).
     * Optimisations by Peter Eastman (peastman@drizzle.stanford.edu).
     * Better rank ordering method by Stefan Gustavson in 2012.
     * Converted to Javascript by Joseph Gentle.
     *
     * Version 2012-03-09
     *
     * This code was placed in the public domain by its original author,
     * Stefan Gustavson. You may use it as you see fit, but
     * attribution is appreciated.
     *
     * @see https://github.com/josephg/noisejs/blob/master/perlin.js
     */
    var PerlinNoise1 = /** @class */ (function () {
        function PerlinNoise1(seed) {
            if (seed === void 0) { seed = 0; }
            // To remove the need for index wrapping, double the permutation table length
            this.perm = [];
            this.gradP = [];
            this._seed = 0;
            this.seed = seed;
        }
        Object.defineProperty(PerlinNoise1.prototype, "seed", {
            // This isn't a very good seeding function, but it works ok. It supports 2^16
            // different seed values. Write something better if you need more seeds.
            get: function () {
                return this._seed;
            },
            set: function (v) {
                this._seed = v;
                if (v > 0 && v < 1) {
                    // Scale the seed out
                    v *= 65536;
                }
                v = Math.floor(v);
                if (v < 256) {
                    v |= v << 8;
                }
                var perm = this.perm;
                var gradP = this.gradP;
                for (var i = 0; i < 256; i++) {
                    var v0;
                    if (i & 1) {
                        v0 = p[i] ^ (v & 255);
                    }
                    else {
                        v0 = p[i] ^ ((v >> 8) & 255);
                    }
                    perm[i] = perm[i + 256] = v0;
                    gradP[i] = gradP[i + 256] = grad3[v0 % 12];
                }
            },
            enumerable: true,
            configurable: true
        });
        // 2D simplex noise
        PerlinNoise1.prototype.simplex2 = function (xin, yin) {
            var perm = this.perm;
            var gradP = this.gradP;
            var n0, n1, n2; // Noise contributions from the three corners
            // Skew the input space to determine which simplex cell we're in
            var s = (xin + yin) * F2; // Hairy factor for 2D
            var i = Math.floor(xin + s);
            var j = Math.floor(yin + s);
            var t = (i + j) * G2;
            var x0 = xin - i + t; // The x,y distances from the cell origin, unskewed.
            var y0 = yin - j + t;
            // For the 2D case, the simplex shape is an equilateral triangle.
            // Determine which simplex we are in.
            var i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if (x0 > y0) { // lower triangle, XY order: (0,0)->(1,0)->(1,1)
                i1 = 1;
                j1 = 0;
            }
            else { // upper triangle, YX order: (0,0)->(0,1)->(1,1)
                i1 = 0;
                j1 = 1;
            }
            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6
            var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            var y1 = y0 - j1 + G2;
            var x2 = x0 - 1 + 2 * G2; // Offsets for last corner in (x,y) unskewed coords
            var y2 = y0 - 1 + 2 * G2;
            // Work out the hashed gradient indices of the three simplex corners
            i &= 255;
            j &= 255;
            var gi0 = gradP[i + perm[j]];
            var gi1 = gradP[i + i1 + perm[j + j1]];
            var gi2 = gradP[i + 1 + perm[j + 1]];
            // Calculate the contribution from the three corners
            var t0 = 0.5 - x0 * x0 - y0 * y0;
            if (t0 < 0) {
                n0 = 0;
            }
            else {
                t0 *= t0;
                n0 = t0 * t0 * gi0.dot2(x0, y0); // (x,y) of grad3 used for 2D gradient
            }
            var t1 = 0.5 - x1 * x1 - y1 * y1;
            if (t1 < 0) {
                n1 = 0;
            }
            else {
                t1 *= t1;
                n1 = t1 * t1 * gi1.dot2(x1, y1);
            }
            var t2 = 0.5 - x2 * x2 - y2 * y2;
            if (t2 < 0) {
                n2 = 0;
            }
            else {
                t2 *= t2;
                n2 = t2 * t2 * gi2.dot2(x2, y2);
            }
            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 70 * (n0 + n1 + n2);
        };
        // 3D simplex noise
        PerlinNoise1.prototype.simplex3 = function (xin, yin, zin) {
            var perm = this.perm;
            var gradP = this.gradP;
            var n0, n1, n2, n3; // Noise contributions from the four corners
            // Skew the input space to determine which simplex cell we're in
            var s = (xin + yin + zin) * F3; // Hairy factor for 2D
            var i = Math.floor(xin + s);
            var j = Math.floor(yin + s);
            var k = Math.floor(zin + s);
            var t = (i + j + k) * G3;
            var x0 = xin - i + t; // The x,y distances from the cell origin, unskewed.
            var y0 = yin - j + t;
            var z0 = zin - k + t;
            // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
            // Determine which simplex we are in.
            var i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
            var i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
            if (x0 >= y0) {
                if (y0 >= z0) {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                }
                else if (x0 >= z0) {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                }
                else {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                }
            }
            else {
                if (y0 < z0) {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                }
                else if (x0 < z0) {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                }
                else {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                }
            }
            // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
            // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
            // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
            // c = 1/6.
            var x1 = x0 - i1 + G3; // Offsets for second corner
            var y1 = y0 - j1 + G3;
            var z1 = z0 - k1 + G3;
            var x2 = x0 - i2 + 2 * G3; // Offsets for third corner
            var y2 = y0 - j2 + 2 * G3;
            var z2 = z0 - k2 + 2 * G3;
            var x3 = x0 - 1 + 3 * G3; // Offsets for fourth corner
            var y3 = y0 - 1 + 3 * G3;
            var z3 = z0 - 1 + 3 * G3;
            // Work out the hashed gradient indices of the four simplex corners
            i &= 255;
            j &= 255;
            k &= 255;
            var gi0 = gradP[i + perm[j + perm[k]]];
            var gi1 = gradP[i + i1 + perm[j + j1 + perm[k + k1]]];
            var gi2 = gradP[i + i2 + perm[j + j2 + perm[k + k2]]];
            var gi3 = gradP[i + 1 + perm[j + 1 + perm[k + 1]]];
            // Calculate the contribution from the four corners
            var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
            if (t0 < 0) {
                n0 = 0;
            }
            else {
                t0 *= t0;
                n0 = t0 * t0 * gi0.dot3(x0, y0, z0); // (x,y) of grad3 used for 2D gradient
            }
            var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
            if (t1 < 0) {
                n1 = 0;
            }
            else {
                t1 *= t1;
                n1 = t1 * t1 * gi1.dot3(x1, y1, z1);
            }
            var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
            if (t2 < 0) {
                n2 = 0;
            }
            else {
                t2 *= t2;
                n2 = t2 * t2 * gi2.dot3(x2, y2, z2);
            }
            var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
            if (t3 < 0) {
                n3 = 0;
            }
            else {
                t3 *= t3;
                n3 = t3 * t3 * gi3.dot3(x3, y3, z3);
            }
            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 32 * (n0 + n1 + n2 + n3);
        };
        // 1D Perlin Noise
        PerlinNoise1.prototype.perlin1 = function (x) {
            var perm = this.perm;
            var gradP = this.gradP;
            // Find unit grid cell containing point
            var X = Math.floor(x);
            // Get relative xy coordinates of point within that cell
            x = x - X;
            // Wrap the integer cells at 255 (smaller integer period can be introduced here)
            X = X & 255;
            // Calculate noise contributions from each of the four corners
            var n0 = gradP[X].dot1(x);
            var n1 = gradP[X + 1].dot1(x - 1);
            // Compute the fade curve value for x
            var u = fade(x);
            // Interpolate the four results
            return lerp(n0, n1, u);
        };
        // 2D Perlin Noise
        PerlinNoise1.prototype.perlin2 = function (x, y) {
            var perm = this.perm;
            var gradP = this.gradP;
            // Find unit grid cell containing point
            var X = Math.floor(x), Y = Math.floor(y);
            // Get relative xy coordinates of point within that cell
            x = x - X;
            y = y - Y;
            // Wrap the integer cells at 255 (smaller integer period can be introduced here)
            X = X & 255;
            Y = Y & 255;
            // Calculate noise contributions from each of the four corners
            var n00 = gradP[X + perm[Y]].dot2(x, y);
            var n01 = gradP[X + perm[Y + 1]].dot2(x, y - 1);
            var n10 = gradP[X + 1 + perm[Y]].dot2(x - 1, y);
            var n11 = gradP[X + 1 + perm[Y + 1]].dot2(x - 1, y - 1);
            // Compute the fade curve value for x
            var u = fade(x);
            var v = fade(y);
            // Interpolate the four results
            return lerp(lerp(n00, n10, u), // nx0
            lerp(n01, n11, u), // nx1
            v); // nxy
        };
        // 3D Perlin Noise
        PerlinNoise1.prototype.perlin3 = function (x, y, z) {
            var perm = this.perm;
            var gradP = this.gradP;
            // Find unit grid cell containing point
            var X = Math.floor(x), Y = Math.floor(y), Z = Math.floor(z);
            // Get relative xyz coordinates of point within that cell
            x = x - X;
            y = y - Y;
            z = z - Z;
            // Wrap the integer cells at 255 (smaller integer period can be introduced here)
            X = X & 255;
            Y = Y & 255;
            Z = Z & 255;
            // Calculate noise contributions from each of the eight corners
            var n000 = gradP[X + perm[Y + perm[Z]]].dot3(x, y, z);
            var n001 = gradP[X + perm[Y + perm[Z + 1]]].dot3(x, y, z - 1);
            var n010 = gradP[X + perm[Y + 1 + perm[Z]]].dot3(x, y - 1, z);
            var n011 = gradP[X + perm[Y + 1 + perm[Z + 1]]].dot3(x, y - 1, z - 1);
            var n100 = gradP[X + 1 + perm[Y + perm[Z]]].dot3(x - 1, y, z);
            var n101 = gradP[X + 1 + perm[Y + perm[Z + 1]]].dot3(x - 1, y, z - 1);
            var n110 = gradP[X + 1 + perm[Y + 1 + perm[Z]]].dot3(x - 1, y - 1, z);
            var n111 = gradP[X + 1 + perm[Y + 1 + perm[Z + 1]]].dot3(x - 1, y - 1, z - 1);
            // Compute the fade curve value for x, y, z
            var u = fade(x);
            var v = fade(y);
            var w = fade(z);
            // Interpolate
            return lerp(lerp(lerp(n000, n100, u), // nx00
            lerp(n010, n110, u), // nx10
            v), // nxy0
            lerp(lerp(n001, n101, u), // nx01
            lerp(n011, n111, u), // nx11
            v), // nxy1
            w); // nxyz
        };
        return PerlinNoise1;
    }());
    feng3d.PerlinNoise1 = PerlinNoise1;
    var Grad = /** @class */ (function () {
        function Grad(x, y, z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        Grad.prototype.dot1 = function (x) {
            return this.x * x;
        };
        Grad.prototype.dot2 = function (x, y) {
            return this.x * x + this.y * y;
        };
        Grad.prototype.dot3 = function (x, y, z) {
            return this.x * x + this.y * y + this.z * z;
        };
        return Grad;
    }());
    var grad3 = [new Grad(1, 1, 0), new Grad(-1, 1, 0), new Grad(1, -1, 0), new Grad(-1, -1, 0),
        new Grad(1, 0, 1), new Grad(-1, 0, 1), new Grad(1, 0, -1), new Grad(-1, 0, -1),
        new Grad(0, 1, 1), new Grad(0, -1, 1), new Grad(0, 1, -1), new Grad(0, -1, -1)];
    var p = [151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180];
    // Skewing and unskewing factors for 2, 3, and 4 dimensions
    var F2 = 0.5 * (Math.sqrt(3) - 1);
    var G2 = (3 - Math.sqrt(3)) / 6;
    var F3 = 1 / 3;
    var G3 = 1 / 6;
    // ##### Perlin noise stuff
    function fade(t) {
        return t * t * t * (t * (t * 6 - 15) + 10);
    }
    function lerp(a, b, t) {
        return (1 - t) * a + t * b;
    }
})(feng3d || (feng3d = {}));
var feng3d;
(function (feng3d) {
    /**
     * Ported from Stefan Gustavson's java implementation
     * http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
     * Read Stefan's excellent paper for details on how this code works.
     *
     * Sean McCullough banksean@gmail.com
     *
     * Added 4D noise
     * Joshua Koo zz85nus@gmail.com
     *
     * @see https://github.com/mrdoob/three.js/blob/dev/examples/js/math/SimplexNoise.js
     */
    var SimplexNoise = /** @class */ (function () {
        /**
         * You can pass in a random number generator object if you like.
         * It is assumed to have a random() method.
         */
        function SimplexNoise(r) {
            if (r == undefined)
                r = Math;
            this._p = [];
            for (var i = 0; i < 256; i++) {
                this._p[i] = Math.floor(r.random() * 256);
            }
            // To remove the need for index wrapping, double the permutation table length
            this._perm = [];
            for (var i = 0; i < 512; i++) {
                this._perm[i] = this._p[i & 255];
            }
        }
        /**
         *
         * @param xin
         * @param yin
         */
        SimplexNoise.prototype.noise = function (xin, yin) {
            var n0, n1, n2; // Noise contributions from the three corners
            // Skew the input space to determine which simplex cell we're in
            var F2 = 0.5 * (Math.sqrt(3.0) - 1.0);
            var s = (xin + yin) * F2; // Hairy factor for 2D
            var i = Math.floor(xin + s);
            var j = Math.floor(yin + s);
            var G2 = (3.0 - Math.sqrt(3.0)) / 6.0;
            var t = (i + j) * G2;
            var X0 = i - t; // Unskew the cell origin back to (x,y) space
            var Y0 = j - t;
            var x0 = xin - X0; // The x,y distances from the cell origin
            var y0 = yin - Y0;
            // For the 2D case, the simplex shape is an equilateral triangle.
            // Determine which simplex we are in.
            var i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if (x0 > y0) {
                i1 = 1;
                j1 = 0;
                // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            }
            else {
                i1 = 0;
                j1 = 1;
            }
            // upper triangle, YX order: (0,0)->(0,1)->(1,1)
            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6
            var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            var y1 = y0 - j1 + G2;
            var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
            var y2 = y0 - 1.0 + 2.0 * G2;
            // Work out the hashed gradient indices of the three simplex corners
            var ii = i & 255;
            var jj = j & 255;
            var gi0 = this._perm[ii + this._perm[jj]] % 12;
            var gi1 = this._perm[ii + i1 + this._perm[jj + j1]] % 12;
            var gi2 = this._perm[ii + 1 + this._perm[jj + 1]] % 12;
            // Calculate the contribution from the three corners
            var t0 = 0.5 - x0 * x0 - y0 * y0;
            if (t0 < 0)
                n0 = 0.0;
            else {
                t0 *= t0;
                n0 = t0 * t0 * _dot(_grad3[gi0], x0, y0); // (x,y) of grad3 used for 2D gradient
            }
            var t1 = 0.5 - x1 * x1 - y1 * y1;
            if (t1 < 0)
                n1 = 0.0;
            else {
                t1 *= t1;
                n1 = t1 * t1 * _dot(_grad3[gi1], x1, y1);
            }
            var t2 = 0.5 - x2 * x2 - y2 * y2;
            if (t2 < 0)
                n2 = 0.0;
            else {
                t2 *= t2;
                n2 = t2 * t2 * _dot(_grad3[gi2], x2, y2);
            }
            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 70.0 * (n0 + n1 + n2);
        };
        /**
         * 3D simplex noise
         *
         * @param xin
         * @param yin
         * @param zin
         */
        SimplexNoise.prototype.noise3d = function (xin, yin, zin) {
            var n0, n1, n2, n3; // Noise contributions from the four corners
            // Skew the input space to determine which simplex cell we're in
            var F3 = 1.0 / 3.0;
            var s = (xin + yin + zin) * F3; // Very nice and simple skew factor for 3D
            var i = Math.floor(xin + s);
            var j = Math.floor(yin + s);
            var k = Math.floor(zin + s);
            var G3 = 1.0 / 6.0; // Very nice and simple unskew factor, too
            var t = (i + j + k) * G3;
            var X0 = i - t; // Unskew the cell origin back to (x,y,z) space
            var Y0 = j - t;
            var Z0 = k - t;
            var x0 = xin - X0; // The x,y,z distances from the cell origin
            var y0 = yin - Y0;
            var z0 = zin - Z0;
            // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
            // Determine which simplex we are in.
            var i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
            var i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
            if (x0 >= y0) {
                if (y0 >= z0) {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                    // X Y Z order
                }
                else if (x0 >= z0) {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                    // X Z Y order
                }
                else {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                } // Z X Y order
            }
            else { // x0<y0
                if (y0 < z0) {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                    // Z Y X order
                }
                else if (x0 < z0) {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                    // Y Z X order
                }
                else {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                } // Y X Z order
            }
            // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
            // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
            // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
            // c = 1/6.
            var x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
            var y1 = y0 - j1 + G3;
            var z1 = z0 - k1 + G3;
            var x2 = x0 - i2 + 2.0 * G3; // Offsets for third corner in (x,y,z) coords
            var y2 = y0 - j2 + 2.0 * G3;
            var z2 = z0 - k2 + 2.0 * G3;
            var x3 = x0 - 1.0 + 3.0 * G3; // Offsets for last corner in (x,y,z) coords
            var y3 = y0 - 1.0 + 3.0 * G3;
            var z3 = z0 - 1.0 + 3.0 * G3;
            // Work out the hashed gradient indices of the four simplex corners
            var ii = i & 255;
            var jj = j & 255;
            var kk = k & 255;
            var gi0 = this._perm[ii + this._perm[jj + this._perm[kk]]] % 12;
            var gi1 = this._perm[ii + i1 + this._perm[jj + j1 + this._perm[kk + k1]]] % 12;
            var gi2 = this._perm[ii + i2 + this._perm[jj + j2 + this._perm[kk + k2]]] % 12;
            var gi3 = this._perm[ii + 1 + this._perm[jj + 1 + this._perm[kk + 1]]] % 12;
            // Calculate the contribution from the four corners
            var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
            if (t0 < 0)
                n0 = 0.0;
            else {
                t0 *= t0;
                n0 = t0 * t0 * _dot3(_grad3[gi0], x0, y0, z0);
            }
            var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
            if (t1 < 0)
                n1 = 0.0;
            else {
                t1 *= t1;
                n1 = t1 * t1 * _dot3(_grad3[gi1], x1, y1, z1);
            }
            var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
            if (t2 < 0)
                n2 = 0.0;
            else {
                t2 *= t2;
                n2 = t2 * t2 * _dot3(_grad3[gi2], x2, y2, z2);
            }
            var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
            if (t3 < 0)
                n3 = 0.0;
            else {
                t3 *= t3;
                n3 = t3 * t3 * _dot3(_grad3[gi3], x3, y3, z3);
            }
            // Add contributions from each corner to get the final noise value.
            // The result is scaled to stay just inside [-1,1]
            return 32.0 * (n0 + n1 + n2 + n3);
        };
        /**
         * 4D simplex noise
         *
         * @param x
         * @param y
         * @param z
         * @param w
         */
        SimplexNoise.prototype.noise4d = function (x, y, z, w) {
            // For faster and easier lookups
            var grad4 = _grad4;
            var simplex = _simplex;
            var perm = this._perm;
            // The skewing and unskewing factors are hairy again for the 4D case
            var F4 = (Math.sqrt(5.0) - 1.0) / 4.0;
            var G4 = (5.0 - Math.sqrt(5.0)) / 20.0;
            var n0, n1, n2, n3, n4; // Noise contributions from the five corners
            // Skew the (x,y,z,w) space to determine which cell of 24 simplices we're in
            var s = (x + y + z + w) * F4; // Factor for 4D skewing
            var i = Math.floor(x + s);
            var j = Math.floor(y + s);
            var k = Math.floor(z + s);
            var l = Math.floor(w + s);
            var t = (i + j + k + l) * G4; // Factor for 4D unskewing
            var X0 = i - t; // Unskew the cell origin back to (x,y,z,w) space
            var Y0 = j - t;
            var Z0 = k - t;
            var W0 = l - t;
            var x0 = x - X0; // The x,y,z,w distances from the cell origin
            var y0 = y - Y0;
            var z0 = z - Z0;
            var w0 = w - W0;
            // For the 4D case, the simplex is a 4D shape I won't even try to describe.
            // To find out which of the 24 possible simplices we're in, we need to
            // determine the magnitude ordering of x0, y0, z0 and w0.
            // The method below is a good way of finding the ordering of x,y,z,w and
            // then find the correct traversal order for the simplex we’re in.
            // First, six pair-wise comparisons are performed between each possible pair
            // of the four coordinates, and the results are used to add up binary bits
            // for an integer index.
            var c1 = (x0 > y0) ? 32 : 0;
            var c2 = (x0 > z0) ? 16 : 0;
            var c3 = (y0 > z0) ? 8 : 0;
            var c4 = (x0 > w0) ? 4 : 0;
            var c5 = (y0 > w0) ? 2 : 0;
            var c6 = (z0 > w0) ? 1 : 0;
            var c = c1 + c2 + c3 + c4 + c5 + c6;
            var i1, j1, k1, l1; // The integer offsets for the second simplex corner
            var i2, j2, k2, l2; // The integer offsets for the third simplex corner
            var i3, j3, k3, l3; // The integer offsets for the fourth simplex corner
            // simplex[c] is a 4-vector with the numbers 0, 1, 2 and 3 in some order.
            // Many values of c will never occur, since e.g. x>y>z>w makes x<z, y<w and x<w
            // impossible. Only the 24 indices which have non-zero entries make any sense.
            // We use a thresholding to set the coordinates in turn from the largest magnitude.
            // The number 3 in the "simplex" array is at the position of the largest coordinate.
            i1 = simplex[c][0] >= 3 ? 1 : 0;
            j1 = simplex[c][1] >= 3 ? 1 : 0;
            k1 = simplex[c][2] >= 3 ? 1 : 0;
            l1 = simplex[c][3] >= 3 ? 1 : 0;
            // The number 2 in the "simplex" array is at the second largest coordinate.
            i2 = simplex[c][0] >= 2 ? 1 : 0;
            j2 = simplex[c][1] >= 2 ? 1 : 0;
            k2 = simplex[c][2] >= 2 ? 1 : 0;
            l2 = simplex[c][3] >= 2 ? 1 : 0;
            // The number 1 in the "simplex" array is at the second smallest coordinate.
            i3 = simplex[c][0] >= 1 ? 1 : 0;
            j3 = simplex[c][1] >= 1 ? 1 : 0;
            k3 = simplex[c][2] >= 1 ? 1 : 0;
            l3 = simplex[c][3] >= 1 ? 1 : 0;
            // The fifth corner has all coordinate offsets = 1, so no need to look that up.
            var x1 = x0 - i1 + G4; // Offsets for second corner in (x,y,z,w) coords
            var y1 = y0 - j1 + G4;
            var z1 = z0 - k1 + G4;
            var w1 = w0 - l1 + G4;
            var x2 = x0 - i2 + 2.0 * G4; // Offsets for third corner in (x,y,z,w) coords
            var y2 = y0 - j2 + 2.0 * G4;
            var z2 = z0 - k2 + 2.0 * G4;
            var w2 = w0 - l2 + 2.0 * G4;
            var x3 = x0 - i3 + 3.0 * G4; // Offsets for fourth corner in (x,y,z,w) coords
            var y3 = y0 - j3 + 3.0 * G4;
            var z3 = z0 - k3 + 3.0 * G4;
            var w3 = w0 - l3 + 3.0 * G4;
            var x4 = x0 - 1.0 + 4.0 * G4; // Offsets for last corner in (x,y,z,w) coords
            var y4 = y0 - 1.0 + 4.0 * G4;
            var z4 = z0 - 1.0 + 4.0 * G4;
            var w4 = w0 - 1.0 + 4.0 * G4;
            // Work out the hashed gradient indices of the five simplex corners
            var ii = i & 255;
            var jj = j & 255;
            var kk = k & 255;
            var ll = l & 255;
            var gi0 = perm[ii + perm[jj + perm[kk + perm[ll]]]] % 32;
            var gi1 = perm[ii + i1 + perm[jj + j1 + perm[kk + k1 + perm[ll + l1]]]] % 32;
            var gi2 = perm[ii + i2 + perm[jj + j2 + perm[kk + k2 + perm[ll + l2]]]] % 32;
            var gi3 = perm[ii + i3 + perm[jj + j3 + perm[kk + k3 + perm[ll + l3]]]] % 32;
            var gi4 = perm[ii + 1 + perm[jj + 1 + perm[kk + 1 + perm[ll + 1]]]] % 32;
            // Calculate the contribution from the five corners
            var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0;
            if (t0 < 0)
                n0 = 0.0;
            else {
                t0 *= t0;
                n0 = t0 * t0 * _dot4(grad4[gi0], x0, y0, z0, w0);
            }
            var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1;
            if (t1 < 0)
                n1 = 0.0;
            else {
                t1 *= t1;
                n1 = t1 * t1 * _dot4(grad4[gi1], x1, y1, z1, w1);
            }
            var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2;
            if (t2 < 0)
                n2 = 0.0;
            else {
                t2 *= t2;
                n2 = t2 * t2 * _dot4(grad4[gi2], x2, y2, z2, w2);
            }
            var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3;
            if (t3 < 0)
                n3 = 0.0;
            else {
                t3 *= t3;
                n3 = t3 * t3 * _dot4(grad4[gi3], x3, y3, z3, w3);
            }
            var t4 = 0.6 - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4;
            if (t4 < 0)
                n4 = 0.0;
            else {
                t4 *= t4;
                n4 = t4 * t4 * _dot4(grad4[gi4], x4, y4, z4, w4);
            }
            // Sum up and scale the result to cover the range [-1,1]
            return 27.0 * (n0 + n1 + n2 + n3 + n4);
        };
        return SimplexNoise;
    }());
    feng3d.SimplexNoise = SimplexNoise;
    var _grad3 = [[1, 1, 0], [-1, 1, 0], [1, -1, 0], [-1, -1, 0],
        [1, 0, 1], [-1, 0, 1], [1, 0, -1], [-1, 0, -1],
        [0, 1, 1], [0, -1, 1], [0, 1, -1], [0, -1, -1]];
    var _grad4 = [[0, 1, 1, 1], [0, 1, 1, -1], [0, 1, -1, 1], [0, 1, -1, -1],
        [0, -1, 1, 1], [0, -1, 1, -1], [0, -1, -1, 1], [0, -1, -1, -1],
        [1, 0, 1, 1], [1, 0, 1, -1], [1, 0, -1, 1], [1, 0, -1, -1],
        [-1, 0, 1, 1], [-1, 0, 1, -1], [-1, 0, -1, 1], [-1, 0, -1, -1],
        [1, 1, 0, 1], [1, 1, 0, -1], [1, -1, 0, 1], [1, -1, 0, -1],
        [-1, 1, 0, 1], [-1, 1, 0, -1], [-1, -1, 0, 1], [-1, -1, 0, -1],
        [1, 1, 1, 0], [1, 1, -1, 0], [1, -1, 1, 0], [1, -1, -1, 0],
        [-1, 1, 1, 0], [-1, 1, -1, 0], [-1, -1, 1, 0], [-1, -1, -1, 0]];
    // A lookup table to traverse the simplex around a given point in 4D.
    // Details can be found where this table is used, in the 4D noise method.
    var _simplex = [
        [0, 1, 2, 3], [0, 1, 3, 2], [0, 0, 0, 0], [0, 2, 3, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 2, 3, 0],
        [0, 2, 1, 3], [0, 0, 0, 0], [0, 3, 1, 2], [0, 3, 2, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 3, 2, 0],
        [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
        [1, 2, 0, 3], [0, 0, 0, 0], [1, 3, 0, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 3, 0, 1], [2, 3, 1, 0],
        [1, 0, 2, 3], [1, 0, 3, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 0, 3, 1], [0, 0, 0, 0], [2, 1, 3, 0],
        [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
        [2, 0, 1, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 0, 1, 2], [3, 0, 2, 1], [0, 0, 0, 0], [3, 1, 2, 0],
        [2, 1, 0, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 1, 0, 2], [0, 0, 0, 0], [3, 2, 0, 1], [3, 2, 1, 0]
    ];
    function _dot(g, x, y) {
        return g[0] * x + g[1] * y;
    }
    function _dot3(g, x, y, z) {
        return g[0] * x + g[1] * y + g[2] * z;
    }
    function _dot4(g, x, y, z, w) {
        return g[0] * x + g[1] * y + g[2] * z + g[3] * w;
    }
})(feng3d || (feng3d = {}));
var feng3d;
(function (feng3d) {
    /**
     *
     * Simplex Noise for C#
     * Copyright © Benjamin Ward 2019
     *
     * See LICENSE
     * Simplex Noise implementation offering 1D, 2D, and 3D forms w/ values in the range of 0 to 255.
     * Based on work by Heikki Törmälä (2012) and Stefan Gustavson (2006).
     *
     * Implementation of the Perlin simplex noise, an improved Perlin noise algorithm.
     * Based loosely on SimplexNoise1234 by Stefan Gustavson: http://staffwww.itn.liu.se/~stegu/aqsis/aqsis-newnoise/
     *
     * @see https://github.com/WardBenjamin/SimplexNoise/blob/master/SimplexNoise/Noise.cs
     */
    var SimplexNoise1 = /** @class */ (function () {
        function SimplexNoise1() {
        }
        SimplexNoise1.prototype.Calc1D = function (width, scale) {
            var values = [];
            for (var i = 0; i < width; i++) {
                values[i] = Generate1(i * scale) * 128 + 128;
            }
            return values;
        };
        SimplexNoise1.prototype.Calc2D = function (width, height, scale) {
            var values = [];
            for (var i = 0; i < width; i++) {
                values[i] = [];
                for (var j = 0; j < height; j++)
                    values[i][j] = Generate2(i * scale, j * scale) * 128 + 128;
            }
            return values;
        };
        SimplexNoise1.prototype.Calc3D = function (width, height, length, scale) {
            var values = [];
            for (var i = 0; i < width; i++) {
                values[i] = [];
                for (var j = 0; j < height; j++) {
                    values[i][j] = [];
                    for (var k = 0; k < length; k++)
                        values[i][j][k] = Generate3(i * scale, j * scale, k * scale) * 128 + 128;
                }
            }
            return values;
        };
        SimplexNoise1.prototype.CalcPixel1D = function (x, scale) {
            return Generate1(x * scale) * 128 + 128;
        };
        SimplexNoise1.prototype.CalcPixel2D = function (x, y, scale) {
            return Generate2(x * scale, y * scale) * 128 + 128;
        };
        SimplexNoise1.prototype.CalcPixel3D = function (x, y, z, scale) {
            return Generate3(x * scale, y * scale, z * scale) * 128 + 128;
        };
        return SimplexNoise1;
    }());
    feng3d.SimplexNoise1 = SimplexNoise1;
    /// <summary>
    /// 1D simplex noise
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    function Generate1(x) {
        var i0 = FastFloor(x);
        var i1 = i0 + 1;
        var x0 = x - i0;
        var x1 = x0 - 1.0;
        var t0 = 1.0 - x0 * x0;
        t0 *= t0;
        var n0 = t0 * t0 * Grad1(_perm[i0 & 0xff], x0);
        var t1 = 1.0 - x1 * x1;
        t1 *= t1;
        var n1 = t1 * t1 * Grad1(_perm[i1 & 0xff], x1);
        // The maximum value of this noise is 8*(3/4)^4 = 2.53125
        // A factor of 0.395 scales to fit exactly within [-1,1]
        return 0.395 * (n0 + n1);
    }
    feng3d.Generate1 = Generate1;
    /// <summary>
    /// 2D simplex noise
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    function Generate2(x, y) {
        var F2 = 0.366025403; // F2 = 0.5*(sqrt(3.0)-1.0)
        var G2 = 0.211324865; // G2 = (3.0-Math.sqrt(3.0))/6.0
        var n0, n1, n2; // Noise contributions from the three corners
        // Skew the input space to determine which simplex cell we're in
        var s = (x + y) * F2; // Hairy factor for 2D
        var xs = x + s;
        var ys = y + s;
        var i = FastFloor(xs);
        var j = FastFloor(ys);
        var t = (i + j) * G2;
        var X0 = i - t; // Unskew the cell origin back to (x,y) space
        var Y0 = j - t;
        var x0 = x - X0; // The x,y distances from the cell origin
        var y0 = y - Y0;
        // For the 2D case, the simplex shape is an equilateral triangle.
        // Determine which simplex we are in.
        var i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
        if (x0 > y0) {
            i1 = 1;
            j1 = 0;
        } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
        else {
            i1 = 0;
            j1 = 1;
        } // upper triangle, YX order: (0,0)->(0,1)->(1,1)
        // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
        // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
        // c = (3-sqrt(3))/6
        var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
        var y1 = y0 - j1 + G2;
        var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
        var y2 = y0 - 1.0 + 2.0 * G2;
        // Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
        var ii = Mod(i, 256);
        var jj = Mod(j, 256);
        // Calculate the contribution from the three corners
        var t0 = 0.5 - x0 * x0 - y0 * y0;
        if (t0 < 0.0)
            n0 = 0.0;
        else {
            t0 *= t0;
            n0 = t0 * t0 * Grad2(_perm[ii + _perm[jj]], x0, y0);
        }
        var t1 = 0.5 - x1 * x1 - y1 * y1;
        if (t1 < 0.0)
            n1 = 0.0;
        else {
            t1 *= t1;
            n1 = t1 * t1 * Grad2(_perm[ii + i1 + _perm[jj + j1]], x1, y1);
        }
        var t2 = 0.5 - x2 * x2 - y2 * y2;
        if (t2 < 0.0)
            n2 = 0.0;
        else {
            t2 *= t2;
            n2 = t2 * t2 * Grad2(_perm[ii + 1 + _perm[jj + 1]], x2, y2);
        }
        // Add contributions from each corner to get the final noise value.
        // The result is scaled to return values in the interval [-1,1].
        return 40.0 * (n0 + n1 + n2); // TODO: The scale factor is preliminary!
    }
    feng3d.Generate2 = Generate2;
    function Generate3(x, y, z) {
        // Simple skewing factors for the 3D case
        var F3 = 0.333333333;
        var G3 = 0.166666667;
        var n0, n1, n2, n3; // Noise contributions from the four corners
        // Skew the input space to determine which simplex cell we're in
        var s = (x + y + z) * F3; // Very nice and simple skew factor for 3D
        var xs = x + s;
        var ys = y + s;
        var zs = z + s;
        var i = FastFloor(xs);
        var j = FastFloor(ys);
        var k = FastFloor(zs);
        var t = (i + j + k) * G3;
        var X0 = i - t; // Unskew the cell origin back to (x,y,z) space
        var Y0 = j - t;
        var Z0 = k - t;
        var x0 = x - X0; // The x,y,z distances from the cell origin
        var y0 = y - Y0;
        var z0 = z - Z0;
        // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
        // Determine which simplex we are in.
        var i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
        var i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
        /* This code would benefit from a backport from the GLSL version! */
        if (x0 >= y0) {
            if (y0 >= z0) {
                i1 = 1;
                j1 = 0;
                k1 = 0;
                i2 = 1;
                j2 = 1;
                k2 = 0;
            } // X Y Z order
            else if (x0 >= z0) {
                i1 = 1;
                j1 = 0;
                k1 = 0;
                i2 = 1;
                j2 = 0;
                k2 = 1;
            } // X Z Y order
            else {
                i1 = 0;
                j1 = 0;
                k1 = 1;
                i2 = 1;
                j2 = 0;
                k2 = 1;
            } // Z X Y order
        }
        else { // x0<y0
            if (y0 < z0) {
                i1 = 0;
                j1 = 0;
                k1 = 1;
                i2 = 0;
                j2 = 1;
                k2 = 1;
            } // Z Y X order
            else if (x0 < z0) {
                i1 = 0;
                j1 = 1;
                k1 = 0;
                i2 = 0;
                j2 = 1;
                k2 = 1;
            } // Y Z X order
            else {
                i1 = 0;
                j1 = 1;
                k1 = 0;
                i2 = 1;
                j2 = 1;
                k2 = 0;
            } // Y X Z order
        }
        // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
        // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
        // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
        // c = 1/6.
        var x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
        var y1 = y0 - j1 + G3;
        var z1 = z0 - k1 + G3;
        var x2 = x0 - i2 + 2.0 * G3; // Offsets for third corner in (x,y,z) coords
        var y2 = y0 - j2 + 2.0 * G3;
        var z2 = z0 - k2 + 2.0 * G3;
        var x3 = x0 - 1.0 + 3.0 * G3; // Offsets for last corner in (x,y,z) coords
        var y3 = y0 - 1.0 + 3.0 * G3;
        var z3 = z0 - 1.0 + 3.0 * G3;
        // Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
        var ii = Mod(i, 256);
        var jj = Mod(j, 256);
        var kk = Mod(k, 256);
        // Calculate the contribution from the four corners
        var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
        if (t0 < 0.0)
            n0 = 0.0;
        else {
            t0 *= t0;
            n0 = t0 * t0 * Grad3(_perm[ii + _perm[jj + _perm[kk]]], x0, y0, z0);
        }
        var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
        if (t1 < 0.0)
            n1 = 0.0;
        else {
            t1 *= t1;
            n1 = t1 * t1 * Grad3(_perm[ii + i1 + _perm[jj + j1 + _perm[kk + k1]]], x1, y1, z1);
        }
        var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
        if (t2 < 0.0)
            n2 = 0.0;
        else {
            t2 *= t2;
            n2 = t2 * t2 * Grad3(_perm[ii + i2 + _perm[jj + j2 + _perm[kk + k2]]], x2, y2, z2);
        }
        var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
        if (t3 < 0.0)
            n3 = 0.0;
        else {
            t3 *= t3;
            n3 = t3 * t3 * Grad3(_perm[ii + 1 + _perm[jj + 1 + _perm[kk + 1]]], x3, y3, z3);
        }
        // Add contributions from each corner to get the final noise value.
        // The result is scaled to stay just inside [-1,1]
        return 32.0 * (n0 + n1 + n2 + n3); // TODO: The scale factor is preliminary!
    }
    feng3d.Generate3 = Generate3;
    var PermOriginal = [
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180,
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    ];
    var _perm = PermOriginal.concat();
    function FastFloor(x) {
        return (x > 0) ? Math.floor(x) : (Math.floor(x) - 1);
    }
    function Mod(x, m) {
        var a = x % m;
        return a < 0 ? a + m : a;
    }
    function Grad1(hash, x) {
        var h = hash & 15;
        var grad = 1.0 + (h & 7); // Gradient value 1.0, 2.0, ..., 8.0
        if ((h & 8) != 0)
            grad = -grad; // Set a random sign for the gradient
        return (grad * x); // Multiply the gradient with the distance
    }
    function Grad2(hash, x, y) {
        var h = hash & 7; // Convert low 3 bits of hash code
        var u = h < 4 ? x : y; // into 8 simple gradient directions,
        var v = h < 4 ? y : x; // and compute the dot product with (x,y).
        return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0 * v : 2.0 * v);
    }
    function Grad3(hash, x, y, z) {
        var h = hash & 15; // Convert low 4 bits of hash code into 12 simple
        var u = h < 8 ? x : y; // gradient directions, and compute dot product.
        var v = h < 4 ? y : h == 12 || h == 14 ? x : z; // Fix repeats at h = 12 to 15
        return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
    }
    function Grad4(hash, x, y, z, t) {
        var h = hash & 31; // Convert low 5 bits of hash code into 32 simple
        var u = h < 24 ? x : y; // gradient directions, and compute dot product.
        var v = h < 16 ? y : z;
        var w = h < 8 ? z : t;
        return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v) + ((h & 4) != 0 ? -w : w);
    }
})(feng3d || (feng3d = {}));
var feng3d;
(function (feng3d) {
    /**
     *
     * @see http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
     */
    var SimplexNoise2 = /** @class */ (function () {
        function SimplexNoise2() {
        }
        // 2D simplex noise
        SimplexNoise2.prototype.noise2 = function (xin, yin) {
            var n0, n1, n2; // Noise contributions from the three corners
            // Skew the input space to determine which simplex cell we're in
            var s = (xin + yin) * F2; // Hairy factor for 2D
            var i = Math.floor(xin + s);
            var j = Math.floor(yin + s);
            var t = (i + j) * G2;
            var X0 = i - t; // Unskew the cell origin back to (x,y) space
            var Y0 = j - t;
            var x0 = xin - X0; // The x,y distances from the cell origin
            var y0 = yin - Y0;
            // For the 2D case, the simplex shape is an equilateral triangle.
            // Determine which simplex we are in.
            var i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if (x0 > y0) {
                i1 = 1;
                j1 = 0;
            } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            else {
                i1 = 0;
                j1 = 1;
            } // upper triangle, YX order: (0,0)->(0,1)->(1,1)
            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6
            var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            var y1 = y0 - j1 + G2;
            var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
            var y2 = y0 - 1.0 + 2.0 * G2;
            // Work out the hashed gradient indices of the three simplex corners
            var ii = i & 255;
            var jj = j & 255;
            var gi0 = perm[ii + perm[jj]] % 12;
            var gi1 = perm[ii + i1 + perm[jj + j1]] % 12;
            var gi2 = perm[ii + 1 + perm[jj + 1]] % 12;
            // Calculate the contribution from the three corners
            var t0 = 0.5 - x0 * x0 - y0 * y0;
            if (t0 < 0)
                n0 = 0.0;
            else {
                t0 *= t0;
                n0 = t0 * t0 * dot2(grad3[gi0], x0, y0); // (x,y) of grad3 used for 2D gradient
            }
            var t1 = 0.5 - x1 * x1 - y1 * y1;
            if (t1 < 0)
                n1 = 0.0;
            else {
                t1 *= t1;
                n1 = t1 * t1 * dot2(grad3[gi1], x1, y1);
            }
            var t2 = 0.5 - x2 * x2 - y2 * y2;
            if (t2 < 0)
                n2 = 0.0;
            else {
                t2 *= t2;
                n2 = t2 * t2 * dot2(grad3[gi2], x2, y2);
            }
            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 70.0 * (n0 + n1 + n2);
        };
        // 3D simplex noise
        SimplexNoise2.prototype.noise3 = function (xin, yin, zin) {
            var n0, n1, n2, n3; // Noise contributions from the four corners
            // Skew the input space to determine which simplex cell we're in
            var s = (xin + yin + zin) * F3; // Very nice and simple skew factor for 3D
            var i = Math.floor(xin + s);
            var j = Math.floor(yin + s);
            var k = Math.floor(zin + s);
            var t = (i + j + k) * G3;
            var X0 = i - t; // Unskew the cell origin back to (x,y,z) space
            var Y0 = j - t;
            var Z0 = k - t;
            var x0 = xin - X0; // The x,y,z distances from the cell origin
            var y0 = yin - Y0;
            var z0 = zin - Z0;
            // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
            // Determine which simplex we are in.
            var i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
            var i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
            if (x0 >= y0) {
                if (y0 >= z0) {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                } // X Y Z order
                else if (x0 >= z0) {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                } // X Z Y order
                else {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                } // Z X Y order
            }
            else { // x0<y0
                if (y0 < z0) {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                } // Z Y X order
                else if (x0 < z0) {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                } // Y Z X order
                else {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                } // Y X Z order
            }
            // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
            // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
            // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
            // c = 1/6.
            var x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
            var y1 = y0 - j1 + G3;
            var z1 = z0 - k1 + G3;
            var x2 = x0 - i2 + 2.0 * G3; // Offsets for third corner in (x,y,z) coords
            var y2 = y0 - j2 + 2.0 * G3;
            var z2 = z0 - k2 + 2.0 * G3;
            var x3 = x0 - 1.0 + 3.0 * G3; // Offsets for last corner in (x,y,z) coords
            var y3 = y0 - 1.0 + 3.0 * G3;
            var z3 = z0 - 1.0 + 3.0 * G3;
            // Work out the hashed gradient indices of the four simplex corners
            var ii = i & 255;
            var jj = j & 255;
            var kk = k & 255;
            var gi0 = perm[ii + perm[jj + perm[kk]]] % 12;
            var gi1 = perm[ii + i1 + perm[jj + j1 + perm[kk + k1]]] % 12;
            var gi2 = perm[ii + i2 + perm[jj + j2 + perm[kk + k2]]] % 12;
            var gi3 = perm[ii + 1 + perm[jj + 1 + perm[kk + 1]]] % 12;
            // Calculate the contribution from the four corners
            var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
            if (t0 < 0)
                n0 = 0.0;
            else {
                t0 *= t0;
                n0 = t0 * t0 * dot3(grad3[gi0], x0, y0, z0);
            }
            var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
            if (t1 < 0)
                n1 = 0.0;
            else {
                t1 *= t1;
                n1 = t1 * t1 * dot3(grad3[gi1], x1, y1, z1);
            }
            var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
            if (t2 < 0)
                n2 = 0.0;
            else {
                t2 *= t2;
                n2 = t2 * t2 * dot3(grad3[gi2], x2, y2, z2);
            }
            var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
            if (t3 < 0)
                n3 = 0.0;
            else {
                t3 *= t3;
                n3 = t3 * t3 * dot3(grad3[gi3], x3, y3, z3);
            }
            // Add contributions from each corner to get the final noise value.
            // The result is scaled to stay just inside [-1,1]
            return 32.0 * (n0 + n1 + n2 + n3);
        };
        // 4D simplex noise
        SimplexNoise2.prototype.noise4 = function (x, y, z, w) {
            var n0, n1, n2, n3, n4; // Noise contributions from the five corners
            // Skew the (x,y,z,w) space to determine which cell of 24 simplices we're in
            var s = (x + y + z + w) * F4; // Factor for 4D skewing
            var i = Math.floor(x + s);
            var j = Math.floor(y + s);
            var k = Math.floor(z + s);
            var l = Math.floor(w + s);
            var t = (i + j + k + l) * G4; // Factor for 4D unskewing
            var X0 = i - t; // Unskew the cell origin back to (x,y,z,w) space
            var Y0 = j - t;
            var Z0 = k - t;
            var W0 = l - t;
            var x0 = x - X0; // The x,y,z,w distances from the cell origin
            var y0 = y - Y0;
            var z0 = z - Z0;
            var w0 = w - W0;
            // For the 4D case, the simplex is a 4D shape I won't even try to describe.
            // To find out which of the 24 possible simplices we're in, we need to
            // determine the magnitude ordering of x0, y0, z0 and w0.
            // The method below is a good way of finding the ordering of x,y,z,w and
            // then find the correct traversal order for the simplex we’re in.
            // First, six pair-wise comparisons are performed between each possible pair
            // of the four coordinates, and the results are used to add up binary bits
            // for an integer index.
            var c1 = (x0 > y0) ? 32 : 0;
            var c2 = (x0 > z0) ? 16 : 0;
            var c3 = (y0 > z0) ? 8 : 0;
            var c4 = (x0 > w0) ? 4 : 0;
            var c5 = (y0 > w0) ? 2 : 0;
            var c6 = (z0 > w0) ? 1 : 0;
            var c = c1 + c2 + c3 + c4 + c5 + c6;
            var i1, j1, k1, l1; // The integer offsets for the second simplex corner
            var i2, j2, k2, l2; // The integer offsets for the third simplex corner
            var i3, j3, k3, l3; // The integer offsets for the fourth simplex corner
            // simplex[c] is a 4-vector with the numbers 0, 1, 2 and 3 in some order.
            // Many values of c will never occur, since e.g. x>y>z>w makes x<z, y<w and x<w
            // impossible. Only the 24 indices which have non-zero entries make any sense.
            // We use a thresholding to set the coordinates in turn from the largest magnitude.
            // The number 3 in the "simplex" array is at the position of the largest coordinate.
            i1 = simplex[c][0] >= 3 ? 1 : 0;
            j1 = simplex[c][1] >= 3 ? 1 : 0;
            k1 = simplex[c][2] >= 3 ? 1 : 0;
            l1 = simplex[c][3] >= 3 ? 1 : 0;
            // The number 2 in the "simplex" array is at the second largest coordinate.
            i2 = simplex[c][0] >= 2 ? 1 : 0;
            j2 = simplex[c][1] >= 2 ? 1 : 0;
            k2 = simplex[c][2] >= 2 ? 1 : 0;
            l2 = simplex[c][3] >= 2 ? 1 : 0;
            // The number 1 in the "simplex" array is at the second smallest coordinate.
            i3 = simplex[c][0] >= 1 ? 1 : 0;
            j3 = simplex[c][1] >= 1 ? 1 : 0;
            k3 = simplex[c][2] >= 1 ? 1 : 0;
            l3 = simplex[c][3] >= 1 ? 1 : 0;
            // The fifth corner has all coordinate offsets = 1, so no need to look that up.
            var x1 = x0 - i1 + G4; // Offsets for second corner in (x,y,z,w) coords
            var y1 = y0 - j1 + G4;
            var z1 = z0 - k1 + G4;
            var w1 = w0 - l1 + G4;
            var x2 = x0 - i2 + 2.0 * G4; // Offsets for third corner in (x,y,z,w) coords
            var y2 = y0 - j2 + 2.0 * G4;
            var z2 = z0 - k2 + 2.0 * G4;
            var w2 = w0 - l2 + 2.0 * G4;
            var x3 = x0 - i3 + 3.0 * G4; // Offsets for fourth corner in (x,y,z,w) coords
            var y3 = y0 - j3 + 3.0 * G4;
            var z3 = z0 - k3 + 3.0 * G4;
            var w3 = w0 - l3 + 3.0 * G4;
            var x4 = x0 - 1.0 + 4.0 * G4; // Offsets for last corner in (x,y,z,w) coords
            var y4 = y0 - 1.0 + 4.0 * G4;
            var z4 = z0 - 1.0 + 4.0 * G4;
            var w4 = w0 - 1.0 + 4.0 * G4;
            // Work out the hashed gradient indices of the five simplex corners
            var ii = i & 255;
            var jj = j & 255;
            var kk = k & 255;
            var ll = l & 255;
            var gi0 = perm[ii + perm[jj + perm[kk + perm[ll]]]] % 32;
            var gi1 = perm[ii + i1 + perm[jj + j1 + perm[kk + k1 + perm[ll + l1]]]] % 32;
            var gi2 = perm[ii + i2 + perm[jj + j2 + perm[kk + k2 + perm[ll + l2]]]] % 32;
            var gi3 = perm[ii + i3 + perm[jj + j3 + perm[kk + k3 + perm[ll + l3]]]] % 32;
            var gi4 = perm[ii + 1 + perm[jj + 1 + perm[kk + 1 + perm[ll + 1]]]] % 32;
            // Calculate the contribution from the five corners
            var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0;
            if (t0 < 0)
                n0 = 0.0;
            else {
                t0 *= t0;
                n0 = t0 * t0 * dot4(grad4[gi0], x0, y0, z0, w0);
            }
            var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1;
            if (t1 < 0)
                n1 = 0.0;
            else {
                t1 *= t1;
                n1 = t1 * t1 * dot4(grad4[gi1], x1, y1, z1, w1);
            }
            var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2;
            if (t2 < 0)
                n2 = 0.0;
            else {
                t2 *= t2;
                n2 = t2 * t2 * dot4(grad4[gi2], x2, y2, z2, w2);
            }
            var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3;
            if (t3 < 0)
                n3 = 0.0;
            else {
                t3 *= t3;
                n3 = t3 * t3 * dot4(grad4[gi3], x3, y3, z3, w3);
            }
            var t4 = 0.6 - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4;
            if (t4 < 0)
                n4 = 0.0;
            else {
                t4 *= t4;
                n4 = t4 * t4 * dot4(grad4[gi4], x4, y4, z4, w4);
            }
            // Sum up and scale the result to cover the range [-1,1]
            return 27.0 * (n0 + n1 + n2 + n3 + n4);
        };
        return SimplexNoise2;
    }());
    feng3d.SimplexNoise2 = SimplexNoise2;
    var F2 = 0.5 * (Math.sqrt(3.0) - 1.0);
    var G2 = (3.0 - Math.sqrt(3.0)) / 6.0;
    var F3 = 1.0 / 3.0;
    var G3 = 1.0 / 6.0; // Very nice and simple unskew factor, too
    // The skewing and unskewing factors are hairy again for the 4D case
    var F4 = (Math.sqrt(5.0) - 1.0) / 4.0;
    var G4 = (5.0 - Math.sqrt(5.0)) / 20.0;
    var grad3 = [
        [1, 1, 0], [-1, 1, 0], [1, -1, 0],
        [-1, -1, 0], [1, 0, 1], [-1, 0, 1],
        [1, 0, -1], [-1, 0, -1], [0, 1, 1],
        [0, -1, 1], [0, 1, -1], [0, -1, -1]
    ];
    var grad4 = [[0, 1, 1, 1], [0, 1, 1, -1], [0, 1, -1, 1], [0, 1, -1, -1],
        [0, -1, 1, 1], [0, -1, 1, -1], [0, -1, -1, 1], [0, -1, -1, -1],
        [1, 0, 1, 1], [1, 0, 1, -1], [1, 0, -1, 1], [1, 0, -1, -1],
        [-1, 0, 1, 1], [-1, 0, 1, -1], [-1, 0, -1, 1], [-1, 0, -1, -1],
        [1, 1, 0, 1], [1, 1, 0, -1], [1, -1, 0, 1], [1, -1, 0, -1],
        [-1, 1, 0, 1], [-1, 1, 0, -1], [-1, -1, 0, 1], [-1, -1, 0, -1],
        [1, 1, 1, 0], [1, 1, -1, 0], [1, -1, 1, 0], [1, -1, -1, 0],
        [-1, 1, 1, 0], [-1, 1, -1, 0], [-1, -1, 1, 0], [-1, -1, -1, 0]];
    var p = [
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    ];
    // To remove the need for index wrapping, double the permutation table length
    var perm = p.concat(p);
    // A lookup table to traverse the simplex around a given point in 4D.
    // Details can be found where this table is used, in the 4D noise method.
    var simplex = [
        [0, 1, 2, 3], [0, 1, 3, 2], [0, 0, 0, 0], [0, 2, 3, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 2, 3, 0],
        [0, 2, 1, 3], [0, 0, 0, 0], [0, 3, 1, 2], [0, 3, 2, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 3, 2, 0],
        [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
        [1, 2, 0, 3], [0, 0, 0, 0], [1, 3, 0, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 3, 0, 1], [2, 3, 1, 0],
        [1, 0, 2, 3], [1, 0, 3, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 0, 3, 1], [0, 0, 0, 0], [2, 1, 3, 0],
        [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
        [2, 0, 1, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 0, 1, 2], [3, 0, 2, 1], [0, 0, 0, 0], [3, 1, 2, 0],
        [2, 1, 0, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 1, 0, 2], [0, 0, 0, 0], [3, 2, 0, 1], [3, 2, 1, 0]
    ];
    function dot2(g, x, y) {
        return g[0] * x + g[1] * y;
    }
    function dot3(g, x, y, z) {
        return g[0] * x + g[1] * y + g[2] * z;
    }
    function dot4(g, x, y, z, w) {
        return g[0] * x + g[1] * y + g[2] * z + g[3] * w;
    }
})(feng3d || (feng3d = {}));
var feng3d;
(function (feng3d) {
    /**
     * Script interface for the Noise Module.
     *
     * The Noise Module allows you to apply turbulence to the movement of your particles. Use the low quality settings to create computationally efficient Noise, or simulate smoother, richer Noise with the higher quality settings. You can also choose to define the behavior of the Noise individually for each axis.
     *
     * 噪音模块
     *
     * 噪音模块允许你将湍流应用到粒子的运动中。使用低质量设置来创建计算效率高的噪音，或者使用高质量设置来模拟更平滑、更丰富的噪音。您还可以选择为每个轴分别定义噪音的行为。
     */
    var ParticleNoiseModule = /** @class */ (function () {
        function ParticleNoiseModule() {
            /**
             * Control the noise separately for each axis.
             *
             * 分别控制每个轴的噪音。
             */
            this.separateAxes = false;
            /**
             * How strong the overall noise effect is.
             *
             * 整体噪音效应有多强。
             */
            this.strength = 1;
            /**
             * Define the strength of the effect on the X axis, when using separateAxes option.
             *
             * 在使用分别控制每个轴时，在X轴上定义效果的强度。
             */
            this.strengthX = 1;
            /**
             * Define the strength of the effect on the Y axis, when using separateAxes option.
             *
             * 在使用分别控制每个轴时，在Y轴上定义效果的强度。
             */
            this.strengthY = 1;
            /**
             * Define the strength of the effect on the Z axis, when using separateAxes option.
             *
             * 在使用分别控制每个轴时，在Z轴上定义效果的强度。
             */
            this.strengthZ = 1;
            /**
             * Low values create soft, smooth noise, and high values create rapidly changing noise.
             *
             * 低值产生柔和、平滑的噪音，高值产生快速变化的噪音。
             */
            this.frequency = 0.5;
            /**
             * Scroll the noise map over the particle system.
             *
             * 在粒子系统上滚动噪音图。
             */
            this.scrollSpeed = 0;
            /**
             * Higher frequency noise will reduce the strength by a proportional amount, if enabled.
             *
             * 如果启用高频率噪音，将按比例减少强度。
             */
            this.damping = true;
            /**
             * Layers of noise that combine to produce final noise.
             *
             * 噪音层数，如果值大于1则最终噪音有多层混合而成。
             */
            this.octaveCount = 1;
            /**
             * When combining each octave, scale the intensity by this amount.
             *
             * 多层噪音的混合比率。
             */
            this.octaveMultiplier = 0.5;
            /**
             * When combining each octave, zoom in by this amount.
             *
             * 相比第一层的frequency缩放值。
             */
            this.octaveScale = 2;
            /**
             * Generate 1D, 2D or 3D noise.
             *
             * 生成一维、二维或三维噪音。
             */
            this.quality = ParticleSystemNoiseQuality.High;
            /**
             * 噪音
             */
            this.noise = new feng3d.Noise();
            // 以下两个值用于与Unity中数据接近
            this._frequencyScale = 5;
            this._strengthScale = 4;
            this._scrollValue = 0;
            this._preScrollTime = 0;
        }
        /**
         * 绘制噪音到图片
         *
         * @param image 图片数据
         */
        ParticleNoiseModule.prototype.drawImage = function (image) {
            var frequency = this._frequencyScale * this.frequency;
            var strength = this.strength * this._strengthScale;
            if (this.damping)
                strength /= this.frequency;
            var data = image.data;
            var imageWidth = image.width;
            var imageHeight = image.height;
            // var datas: number[] = [];
            // var min = Number.MAX_VALUE;
            // var max = Number.MIN_VALUE;
            for (var x = 0; x < imageWidth; x++) {
                for (var y = 0; y < imageHeight; y++) {
                    var xv = x / imageWidth * frequency;
                    var yv = 1 - y / imageHeight * frequency;
                    var value = this._getNoiseValue(xv, yv);
                    // datas.push(value);
                    // if (min > value) min = value;
                    // if (max < value) max = value;
                    value = (value * strength + 1) / 2 * 256;
                    var cell = (x + y * imageWidth) * 4;
                    data[cell] = data[cell + 1] = data[cell + 2] = Math.floor(value);
                    data[cell + 3] = 255; // alpha
                }
            }
            // console.log(datas, min, max);
        };
        /**
         * 获取噪音值
         *
         * @param x
         * @param y
         */
        ParticleNoiseModule.prototype._getNoiseValue = function (x, y) {
            var value = this._getNoiseValueBase(x, y);
            for (var l = 1, ln = this.octaveCount; l < ln; l++) {
                var value0 = this._getNoiseValueBase(x * this.octaveScale, y * this.octaveScale);
                value += (value0 - value) * this.octaveMultiplier;
            }
            return value;
        };
        /**
         * 获取单层噪音值
         *
         * @param x
         * @param y
         */
        ParticleNoiseModule.prototype._getNoiseValueBase = function (x, y) {
            var scrollValue = this._scrollValue;
            if (this.quality == ParticleSystemNoiseQuality.Low) {
                return this.noise.perlin1(x + scrollValue);
            }
            if (this.quality == ParticleSystemNoiseQuality.Medium) {
                return this.noise.perlin2(x, y + scrollValue);
            }
            // if (this.quality == ParticleSystemNoiseQuality.High)
            return this.noise.perlin3(x, y, 0 + scrollValue);
        };
        ParticleNoiseModule.prototype.update = function () {
            var now = Date.now();
            if (Math.abs(now - this._preScrollTime) < 1000) {
                this._scrollValue += this.scrollSpeed * (now - this._preScrollTime) / 1000;
            }
            this._preScrollTime = now;
        };
        return ParticleNoiseModule;
    }());
    feng3d.ParticleNoiseModule = ParticleNoiseModule;
    /**
     * The quality of the generated noise.
     *
     * 产生的噪音的质量。
     */
    var ParticleSystemNoiseQuality;
    (function (ParticleSystemNoiseQuality) {
        /**
         * Low quality 1D noise.
         *
         * 低质量的一维噪音。
         */
        ParticleSystemNoiseQuality[ParticleSystemNoiseQuality["Low"] = 0] = "Low";
        /**
         * Medium quality 2D noise.
         *
         * 中等质量2D噪音。
         */
        ParticleSystemNoiseQuality[ParticleSystemNoiseQuality["Medium"] = 1] = "Medium";
        /**
         * High quality 3D noise.
         *
         * 高品质3D噪音。
         */
        ParticleSystemNoiseQuality[ParticleSystemNoiseQuality["High"] = 2] = "High";
    })(ParticleSystemNoiseQuality = feng3d.ParticleSystemNoiseQuality || (feng3d.ParticleSystemNoiseQuality = {}));
})(feng3d || (feng3d = {}));
(function () {
    var noise = new feng3d.Noise();
    var NUM = 100;
    console.log("test ClassicNoise.perlin1 ClassicNoise.perlinN");
    var errorNum = 0;
    for (var i = 0; i < NUM; i++) {
        var r = Math.random();
        var v0 = noise.perlin1(r);
        var v1 = noise.perlinN(r);
        if (v0 != v1) {
            errorNum++;
        }
    }
    console.log("ClassicNoise.perlinN \u5B9E\u73B0 ClassicNoise.perlin1 " + (errorNum == 0 ? "成功" : "错误") + "\uFF01");
    console.log("test ClassicNoise.perlin2 ClassicNoise.perlinN");
    var errorNum = 0;
    for (var i = 0; i < NUM; i++) {
        var r0 = Math.random();
        var r1 = Math.random();
        var v0 = noise.perlin2(r0, r1);
        var v1 = noise.perlinN(r0, r1);
        if (v0 != v1) {
            errorNum++;
        }
    }
    console.log("ClassicNoise.perlinN \u5B9E\u73B0 ClassicNoise.perlin2 " + (errorNum == 0 ? "成功" : "错误") + "\uFF01");
    console.log("test ClassicNoise.perlin3 ClassicNoise.perlinN");
    var errorNum = 0;
    for (var i = 0; i < NUM; i++) {
        var r0 = Math.random();
        var r1 = Math.random();
        var r2 = Math.random();
        var v0 = noise.perlin3(r0, r1, r2);
        var v1 = noise.perlinN(r0, r1, r2);
        if (v0 != v1) {
            errorNum++;
        }
    }
    console.log("ClassicNoise.perlinN \u5B9E\u73B0 ClassicNoise.perlin3 " + (errorNum == 0 ? "成功" : "错误") + "\uFF01");
    NUM = 1000000;
    console.log("test ClassicNoise.perlin3 ClassicNoise.perlinN \u6027\u80FD");
    var parms = [];
    for (var i = 0; i < NUM; i++) {
        var r0 = Math.random();
        var r1 = Math.random();
        var r2 = Math.random();
        parms.push([r0, r1, r2]);
    }
    var t1 = Date.now();
    for (var i = 0; i < NUM; i++) {
        var v0 = noise.perlin3(parms[i][0], parms[i][1], parms[i][2]);
    }
    t1 = Date.now() - t1;
    var t2 = Date.now();
    for (var i = 0; i < NUM; i++) {
        var v0 = noise.perlinN(parms[i][0], parms[i][1], parms[i][2]);
    }
    t2 = Date.now() - t2;
    console.log("ClassicNoise.perlinN(" + t2 + ") / ClassicNoise.perlin3(" + t1 + ") " + t2 / t1 + "\uFF01");
})();
//# sourceMappingURL=noise.js.map