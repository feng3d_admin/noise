declare namespace feng3d {
    /**
     * 柏林噪音
     *
     * @see http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
     * @see https://mrl.nyu.edu/~perlin/noise/
     *
     */
    class Noise {
        /**
         * 构建柏林噪音
         *
         * @param seed 随机种子
         */
        constructor(seed?: number);
        /**
         * 1D 经典噪音
         *
         * @param x X轴数值
         */
        perlin1(x: number): number;
        /**
         * 2D 经典噪音
         *
         * @param x X轴数值
         * @param y Y轴数值
         */
        perlin2(x: number, y: number): number;
        /**
         * 3D 经典噪音
         *
         * @param x X轴数值
         * @param y Y轴数值
         * @param z Z轴数值
         */
        perlin3(x: number, y: number, z: number): number;
        /**
         * N阶经典噪音
         *
         * 如果是1D，2D，3D噪音，最好选用对于函数，perlinN中存在for循环因此效率比perlin3等性能差3到5（8）倍！
         *
         * 满足以下运算
         * perlinN(x) == perlin1(x)
         * perlinN(x,y) == perlin2(x,y)
         * perlinN(x,y,z) == perlin3(x,y,z)
         *
         * @param ps 每个轴的数值
         */
        perlinN(...ps: number[]): number;
        /**
         * This isn't a very good seeding function, but it works ok. It supports 2^16
         * different seed values. Write something better if you need more seeds.
         */
        seed: number;
        private _seed;
        private _p;
    }
    /**
     *
     * @param n
     *
     * len = 2^(n-1) * n
     */
    function createGrad(n: number): number[][];
    function getBits(n: number): number[][];
}
declare namespace feng3d {
    class PerlinNoise {
        constructor(seed?: number);
        /**
         *
         * @param x
         */
        perlin1(x: number): number;
        /**
         * Unity中曾使用该算法
         *
         * @param x
         * @param y
         */
        perlin2(x: number, y: number): number;
        /**
         *
         *
         * This code implements the algorithm I describe in a corresponding SIGGRAPH 2002 paper.
         *
         * JAVA REFERENCE IMPLEMENTATION OF IMPROVED NOISE - COPYRIGHT 2002 KEN PERLIN.
         *
         * @see https://mrl.nyu.edu/~perlin/noise/
         */
        perlin3(x: number, y: number, z: number): number;
        /**
         * This isn't a very good seeding function, but it works ok. It supports 2^16
         * different seed values. Write something better if you need more seeds.
         */
        seed: number;
        private _seed;
        private _p;
    }
}
declare namespace feng3d {
    /**
     * A speed-improved perlin and simplex noise algorithms for 2D.
     *
     * Based on example code by Stefan Gustavson (stegu@itn.liu.se).
     * Optimisations by Peter Eastman (peastman@drizzle.stanford.edu).
     * Better rank ordering method by Stefan Gustavson in 2012.
     * Converted to Javascript by Joseph Gentle.
     *
     * Version 2012-03-09
     *
     * This code was placed in the public domain by its original author,
     * Stefan Gustavson. You may use it as you see fit, but
     * attribution is appreciated.
     *
     * @see https://github.com/josephg/noisejs/blob/master/perlin.js
     */
    class PerlinNoise1 {
        perm: number[];
        gradP: Grad[];
        constructor(seed?: number);
        seed: number;
        private _seed;
        simplex2(xin: number, yin: number): number;
        simplex3(xin: number, yin: number, zin: number): number;
        perlin1(x: number): number;
        perlin2(x: number, y: number): number;
        perlin3(x: number, y: number, z: number): number;
    }
    class Grad {
        x: number;
        y: number;
        z: number;
        constructor(x: number, y: number, z: number);
        dot1(x: number): number;
        dot2(x: number, y: number): number;
        dot3(x: number, y: number, z: number): number;
    }
}
declare namespace feng3d {
    /**
     * Ported from Stefan Gustavson's java implementation
     * http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
     * Read Stefan's excellent paper for details on how this code works.
     *
     * Sean McCullough banksean@gmail.com
     *
     * Added 4D noise
     * Joshua Koo zz85nus@gmail.com
     *
     * @see https://github.com/mrdoob/three.js/blob/dev/examples/js/math/SimplexNoise.js
     */
    class SimplexNoise {
        private _p;
        private _perm;
        /**
         * You can pass in a random number generator object if you like.
         * It is assumed to have a random() method.
         */
        constructor(r?: {
            random: () => number;
        });
        /**
         *
         * @param xin
         * @param yin
         */
        noise(xin: number, yin: number): number;
        /**
         * 3D simplex noise
         *
         * @param xin
         * @param yin
         * @param zin
         */
        noise3d(xin: number, yin: number, zin: number): number;
        /**
         * 4D simplex noise
         *
         * @param x
         * @param y
         * @param z
         * @param w
         */
        noise4d(x: number, y: number, z: number, w: number): number;
    }
}
declare namespace feng3d {
    /**
     *
     * Simplex Noise for C#
     * Copyright © Benjamin Ward 2019
     *
     * See LICENSE
     * Simplex Noise implementation offering 1D, 2D, and 3D forms w/ values in the range of 0 to 255.
     * Based on work by Heikki Törmälä (2012) and Stefan Gustavson (2006).
     *
     * Implementation of the Perlin simplex noise, an improved Perlin noise algorithm.
     * Based loosely on SimplexNoise1234 by Stefan Gustavson: http://staffwww.itn.liu.se/~stegu/aqsis/aqsis-newnoise/
     *
     * @see https://github.com/WardBenjamin/SimplexNoise/blob/master/SimplexNoise/Noise.cs
     */
    class SimplexNoise1 {
        Calc1D(width: number, scale: number): number[];
        Calc2D(width: number, height: number, scale: number): number[][];
        Calc3D(width: number, height: number, length: number, scale: number): number[][][];
        CalcPixel1D(x: number, scale: number): number;
        CalcPixel2D(x: number, y: number, scale: number): number;
        CalcPixel3D(x: number, y: number, z: number, scale: number): number;
    }
    function Generate1(x: number): number;
    function Generate2(x: number, y: number): number;
    function Generate3(x: number, y: number, z: number): number;
}
declare namespace feng3d {
    /**
     *
     * @see http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
     */
    class SimplexNoise2 {
        noise2(xin: number, yin: number): number;
        noise3(xin: number, yin: number, zin: number): number;
        noise4(x: number, y: number, z: number, w: number): number;
    }
}
declare namespace feng3d {
    /**
     * Script interface for the Noise Module.
     *
     * The Noise Module allows you to apply turbulence to the movement of your particles. Use the low quality settings to create computationally efficient Noise, or simulate smoother, richer Noise with the higher quality settings. You can also choose to define the behavior of the Noise individually for each axis.
     *
     * 噪音模块
     *
     * 噪音模块允许你将湍流应用到粒子的运动中。使用低质量设置来创建计算效率高的噪音，或者使用高质量设置来模拟更平滑、更丰富的噪音。您还可以选择为每个轴分别定义噪音的行为。
     */
    class ParticleNoiseModule {
        /**
         * Control the noise separately for each axis.
         *
         * 分别控制每个轴的噪音。
         */
        separateAxes: boolean;
        /**
         * How strong the overall noise effect is.
         *
         * 整体噪音效应有多强。
         */
        strength: number;
        /**
         * Define the strength of the effect on the X axis, when using separateAxes option.
         *
         * 在使用分别控制每个轴时，在X轴上定义效果的强度。
         */
        strengthX: number;
        /**
         * Define the strength of the effect on the Y axis, when using separateAxes option.
         *
         * 在使用分别控制每个轴时，在Y轴上定义效果的强度。
         */
        strengthY: number;
        /**
         * Define the strength of the effect on the Z axis, when using separateAxes option.
         *
         * 在使用分别控制每个轴时，在Z轴上定义效果的强度。
         */
        strengthZ: number;
        /**
         * Low values create soft, smooth noise, and high values create rapidly changing noise.
         *
         * 低值产生柔和、平滑的噪音，高值产生快速变化的噪音。
         */
        frequency: number;
        /**
         * Scroll the noise map over the particle system.
         *
         * 在粒子系统上滚动噪音图。
         */
        scrollSpeed: number;
        /**
         * Higher frequency noise will reduce the strength by a proportional amount, if enabled.
         *
         * 如果启用高频率噪音，将按比例减少强度。
         */
        damping: boolean;
        /**
         * Layers of noise that combine to produce final noise.
         *
         * 噪音层数，如果值大于1则最终噪音有多层混合而成。
         */
        octaveCount: number;
        /**
         * When combining each octave, scale the intensity by this amount.
         *
         * 多层噪音的混合比率。
         */
        octaveMultiplier: number;
        /**
         * When combining each octave, zoom in by this amount.
         *
         * 相比第一层的frequency缩放值。
         */
        octaveScale: number;
        /**
         * Generate 1D, 2D or 3D noise.
         *
         * 生成一维、二维或三维噪音。
         */
        quality: ParticleSystemNoiseQuality;
        /**
         * 噪音
         */
        noise: Noise;
        private _frequencyScale;
        private _strengthScale;
        /**
         * 绘制噪音到图片
         *
         * @param image 图片数据
         */
        drawImage(image: ImageData): void;
        /**
         * 获取噪音值
         *
         * @param x
         * @param y
         */
        private _getNoiseValue;
        /**
         * 获取单层噪音值
         *
         * @param x
         * @param y
         */
        private _getNoiseValueBase;
        update(): void;
        private _scrollValue;
        private _preScrollTime;
    }
    /**
     * The quality of the generated noise.
     *
     * 产生的噪音的质量。
     */
    enum ParticleSystemNoiseQuality {
        /**
         * Low quality 1D noise.
         *
         * 低质量的一维噪音。
         */
        Low = 0,
        /**
         * Medium quality 2D noise.
         *
         * 中等质量2D噪音。
         */
        Medium = 1,
        /**
         * High quality 3D noise.
         *
         * 高品质3D噪音。
         */
        High = 2
    }
}
//# sourceMappingURL=noise.d.ts.map