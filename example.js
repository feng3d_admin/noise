/// <reference path="out/noise.d.ts" />

var opts = { imageSize: 100 };

/*
 * START!
 */
function init()
{
    // Dat GUI Options
    var noiseModule = new feng3d.ParticleNoiseModule();

    var gui = new dat.GUI();
    gui.add(opts, 'imageSize', 16, 1024).step(1)
        .onChange(draw);
    gui.add(noiseModule, 'strength', -50, 50)
        .onChange(draw)
    // gui.add(noiseModule, '_strengthScale', 0.1, 40)
    //     .onChange(draw)
    gui.add(noiseModule, 'frequency', 0.05, 50).step(0.01)
        .onChange(draw);
    gui.add(noiseModule, 'scrollSpeed', 0, 5).step(0.01)
        .onChange(draw);
    // gui.add(noiseModule, '_frequencyScale', 0.1, 0.3)
    //     .onChange(draw)
    gui.add(noiseModule, 'damping')
        .onChange(draw);
    gui.add(noiseModule, 'octaveCount', 1, 4).step(1)
        .onChange(draw);
    gui.add(noiseModule, 'octaveMultiplier', 0, 1).step(0.01)
        .onChange(draw);
    gui.add(noiseModule, 'octaveScale', 1, 4).step(0.01)
        .onChange(draw);
    gui.add(noiseModule, "quality", [0, 1, 2]).onChange(draw)

    gui.add(noiseModule.noise, 'seed', 0, 100).step(1)
        .onChange(draw)
    // .onFinishChange(drawCanvas);


    draw();

    function draw()
    {
        var imageSizeX = opts.imageSize;
        var imageSizeY = opts.imageSize;

        console.time("drawCanvas");
        var canvas = document.querySelector('canvas');
        if (!canvas) return;

        //
        canvas.width = imageSizeX;
        canvas.height = imageSizeY;
        var ctx = canvas.getContext('2d');
        if (!ctx) return;

        var image = ctx.createImageData(imageSizeX, imageSizeY);

        noiseModule.drawImage(image);

        ctx.putImageData(image, 0, 0);
        console.timeEnd("drawCanvas");

        noiseModule.update();

        requestAnimationFrame(draw)
    }
}
init();