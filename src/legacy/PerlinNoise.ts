namespace feng3d
{
    export class PerlinNoise
    {

        constructor(seed = 0)
        {
            this.seed = seed;
        }

        /**
         * 
         * @param x 
         */
        perlin1(x: number)
        {
            var p = this._p;

            x = Math.abs(x);

            var floorX = Math.floor(x);

            var X = floorX & 255;                  // FIND UNIT CUBE THAT
            x -= floorX;                                // FIND RELATIVE X,Y,Z
            var u = fade(Math.min(x, 1.0));          // COMPUTE FADE CURVES
            var A = p[X],      // HASH COORDINATES OF
                B = p[X + 1];      // THE 8 CUBE CORNERS,

            var res = lerp(u,
                grad1(p[A], x),  // AND ADD
                grad1(p[B], x - 1));// FROM  8
            return res;
        }

        /**
         * Unity中曾使用该算法
         * 
         * @param x 
         * @param y 
         */
        perlin2(x: number, y: number)
        {
            var p = this._p;

            x = Math.abs(x);
            y = Math.abs(y);

            var floorX = Math.floor(x);
            var floorY = Math.floor(y);

            var X = floorX & 255;                  // FIND UNIT CUBE THAT
            var Y = floorY & 255;                  // CONTAINS POINT.
            x -= floorX;                                // FIND RELATIVE X,Y,Z
            y -= floorY;                                // OF POINT IN CUBE.
            var u = fade(Math.min(x, 1.0));          // COMPUTE FADE CURVES
            var v = fade(Math.min(y, 1.0));          // FOR EACH OF X,Y,Z.
            var A = p[X] + Y, AA = p[A], AB = p[A + 1],      // HASH COORDINATES OF
                B = p[X + 1] + Y, BA = p[B], BB = p[B + 1];      // THE 8 CUBE CORNERS,

            var res = lerp(v, lerp(u, grad2(p[AA], x, y),  // AND ADD
                grad2(p[BA], x - 1, y)), // BLENDED
                lerp(u, grad2(p[AB], x, y - 1),  // RESULTS
                    grad2(p[BB], x - 1, y - 1)));// FROM  8
            return res;
        }

        /**
         * 
         * 
         * This code implements the algorithm I describe in a corresponding SIGGRAPH 2002 paper. 
         * 
         * JAVA REFERENCE IMPLEMENTATION OF IMPROVED NOISE - COPYRIGHT 2002 KEN PERLIN.
         * 
         * @see https://mrl.nyu.edu/~perlin/noise/
         */
        perlin3(x: number, y: number, z: number)
        {
            var p = this._p;

            var X = Math.floor(x) & 255,                  // FIND UNIT CUBE THAT
                Y = Math.floor(y) & 255,                  // CONTAINS POINT.
                Z = Math.floor(z) & 255;
            x -= Math.floor(x);                                // FIND RELATIVE X,Y,Z
            y -= Math.floor(y);                                // OF POINT IN CUBE.
            z -= Math.floor(z);
            var u = fade(x),                                // COMPUTE FADE CURVES
                v = fade(y),                                // FOR EACH OF X,Y,Z.
                w = fade(z);
            var A = p[X] + Y, AA = p[A] + Z, AB = p[A + 1] + Z,      // HASH COORDINATES OF
                B = p[X + 1] + Y, BA = p[B] + Z, BB = p[B + 1] + Z;      // THE 8 CUBE CORNERS,

            return lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z),  // AND ADD
                grad(p[BA], x - 1, y, z)), // BLENDED
                lerp(u, grad(p[AB], x, y - 1, z),  // RESULTS
                    grad(p[BB], x - 1, y - 1, z))),// FROM  8
                lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1),  // CORNERS
                    grad(p[BA + 1], x - 1, y, z - 1)), // OF CUBE
                    lerp(u, grad(p[AB + 1], x, y - 1, z - 1),
                        grad(p[BB + 1], x - 1, y - 1, z - 1))));
        }


        /**
         * This isn't a very good seeding function, but it works ok. It supports 2^16
         * different seed values. Write something better if you need more seeds.
         */
        get seed()
        {
            return this._seed;
        }
        set seed(v)
        {
            this._seed = v;

            var p = this._p;
            if (v > 0 && v < 1)
            {
                // Scale the seed out
                v *= 65536;
            }

            v = Math.floor(v);
            if (v < 256)
            {
                v |= v << 8;
            }

            for (var i = 0; i < 256; i++)
            {
                var v0;
                if (i & 1)
                {
                    v0 = permutation[i] ^ (v & 255);
                } else
                {
                    v0 = permutation[i] ^ ((v >> 8) & 255);
                }

                p[i] = p[i + 256] = v0;
            }
        }
        private _seed: number = 0;
        private _p: number[] = [];
    }

    var permutation = [
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    ];

    function fade(t: number) { return t * t * t * (t * (t * 6 - 15) + 10); }
    function lerp(t: number, a: number, b: number) { return a + t * (b - a); }

    /**
     * 没看懂？
     * 
     * @param hash 
     * @param x 
     * @param y 
     * @param z 
     */
    function grad(hash: number, x: number, y: number, z: number)
    {
        var h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
        var u = h < 8 ? x : y,                 // INTO 12 GRADIENT DIRECTIONS.
            v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
    /**
     * unity中使用？
     * 
     * @param hash 
     * @param x 
     * @param y 
     */
    function grad2(hash: number, x: number, y: number)
    {
        var z = 0;

        var h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
        var u = h < 8 ? x : y,                 // INTO 12 GRADIENT DIRECTIONS.
            v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }

    /**
     * 这么写？
     * 
     * @param hash 
     * @param x 
     */
    function grad1(hash: number, x: number)
    {
        var y = 0, z = 0;

        var h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
        var u = h < 8 ? x : y,                 // INTO 12 GRADIENT DIRECTIONS.
            v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }
}