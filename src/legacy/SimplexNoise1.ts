namespace feng3d
{
    /**
     * 
     * Simplex Noise for C#
     * Copyright © Benjamin Ward 2019
     * 
     * See LICENSE
     * Simplex Noise implementation offering 1D, 2D, and 3D forms w/ values in the range of 0 to 255.
     * Based on work by Heikki Törmälä (2012) and Stefan Gustavson (2006).
     * 
     * Implementation of the Perlin simplex noise, an improved Perlin noise algorithm.
     * Based loosely on SimplexNoise1234 by Stefan Gustavson: http://staffwww.itn.liu.se/~stegu/aqsis/aqsis-newnoise/
     * 
     * @see https://github.com/WardBenjamin/SimplexNoise/blob/master/SimplexNoise/Noise.cs
     */
    export class SimplexNoise1
    {
        Calc1D(width: number, scale: number)
        {
            var values: number[] = [];
            for (var i = 0; i < width; i++)
            {
                values[i] = Generate1(i * scale) * 128 + 128;
            }
            return values;
        }

        Calc2D(width: number, height: number, scale: number)
        {
            var values: number[][] = [];
            for (var i = 0; i < width; i++)
            {
                values[i] = [];
                for (var j = 0; j < height; j++)
                    values[i][j] = Generate2(i * scale, j * scale) * 128 + 128;
            }
            return values;
        }

        Calc3D(width: number, height: number, length: number, scale: number)
        {
            var values: number[][][] = [];
            for (var i = 0; i < width; i++)
            {
                values[i] = [];
                for (var j = 0; j < height; j++)
                {
                    values[i][j] = [];
                    for (var k = 0; k < length; k++)
                        values[i][j][k] = Generate3(i * scale, j * scale, k * scale) * 128 + 128;
                }
            }
            return values;
        }

        CalcPixel1D(x: number, scale: number)
        {
            return Generate1(x * scale) * 128 + 128;
        }

        CalcPixel2D(x: number, y: number, scale: number)
        {
            return Generate2(x * scale, y * scale) * 128 + 128;
        }

        CalcPixel3D(x: number, y: number, z: number, scale: number)
        {
            return Generate3(x * scale, y * scale, z * scale) * 128 + 128;
        }
    }

    /// <summary>
    /// 1D simplex noise
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    export function Generate1(x: number)
    {
        var i0 = FastFloor(x);
        var i1 = i0 + 1;
        var x0 = x - i0;
        var x1 = x0 - 1.0;

        var t0 = 1.0 - x0 * x0;
        t0 *= t0;
        var n0 = t0 * t0 * Grad1(_perm[i0 & 0xff], x0);

        var t1 = 1.0 - x1 * x1;
        t1 *= t1;
        var n1 = t1 * t1 * Grad1(_perm[i1 & 0xff], x1);
        // The maximum value of this noise is 8*(3/4)^4 = 2.53125
        // A factor of 0.395 scales to fit exactly within [-1,1]
        return 0.395 * (n0 + n1);
    }

    /// <summary>
    /// 2D simplex noise
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    export function Generate2(x: number, y: number)
    {
        const F2 = 0.366025403; // F2 = 0.5*(sqrt(3.0)-1.0)
        const G2 = 0.211324865; // G2 = (3.0-Math.sqrt(3.0))/6.0

        var n0: number, n1: number, n2: number; // Noise contributions from the three corners

        // Skew the input space to determine which simplex cell we're in
        var s = (x + y) * F2; // Hairy factor for 2D
        var xs = x + s;
        var ys = y + s;
        var i = FastFloor(xs);
        var j = FastFloor(ys);

        var t = (i + j) * G2;
        var X0 = i - t; // Unskew the cell origin back to (x,y) space
        var Y0 = j - t;
        var x0 = x - X0; // The x,y distances from the cell origin
        var y0 = y - Y0;

        // For the 2D case, the simplex shape is an equilateral triangle.
        // Determine which simplex we are in.
        var i1: number, j1: number; // Offsets for second (middle) corner of simplex in (i,j) coords
        if (x0 > y0) { i1 = 1; j1 = 0; } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
        else { i1 = 0; j1 = 1; }      // upper triangle, YX order: (0,0)->(0,1)->(1,1)

        // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
        // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
        // c = (3-sqrt(3))/6

        var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
        var y1 = y0 - j1 + G2;
        var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
        var y2 = y0 - 1.0 + 2.0 * G2;

        // Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
        var ii = Mod(i, 256);
        var jj = Mod(j, 256);

        // Calculate the contribution from the three corners
        var t0 = 0.5 - x0 * x0 - y0 * y0;
        if (t0 < 0.0) n0 = 0.0;
        else
        {
            t0 *= t0;
            n0 = t0 * t0 * Grad2(_perm[ii + _perm[jj]], x0, y0);
        }

        var t1 = 0.5 - x1 * x1 - y1 * y1;
        if (t1 < 0.0) n1 = 0.0;
        else
        {
            t1 *= t1;
            n1 = t1 * t1 * Grad2(_perm[ii + i1 + _perm[jj + j1]], x1, y1);
        }

        var t2 = 0.5 - x2 * x2 - y2 * y2;
        if (t2 < 0.0) n2 = 0.0;
        else
        {
            t2 *= t2;
            n2 = t2 * t2 * Grad2(_perm[ii + 1 + _perm[jj + 1]], x2, y2);
        }

        // Add contributions from each corner to get the final noise value.
        // The result is scaled to return values in the interval [-1,1].
        return 40.0 * (n0 + n1 + n2); // TODO: The scale factor is preliminary!
    }

    export function Generate3(x: number, y: number, z: number)
    {
        // Simple skewing factors for the 3D case
        const F3 = 0.333333333;
        const G3 = 0.166666667;

        var n0: number, n1: number, n2: number, n3: number; // Noise contributions from the four corners

        // Skew the input space to determine which simplex cell we're in
        var s = (x + y + z) * F3; // Very nice and simple skew factor for 3D
        var xs = x + s;
        var ys = y + s;
        var zs = z + s;
        var i = FastFloor(xs);
        var j = FastFloor(ys);
        var k = FastFloor(zs);

        var t = (i + j + k) * G3;
        var X0 = i - t; // Unskew the cell origin back to (x,y,z) space
        var Y0 = j - t;
        var Z0 = k - t;
        var x0 = x - X0; // The x,y,z distances from the cell origin
        var y0 = y - Y0;
        var z0 = z - Z0;

        // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
        // Determine which simplex we are in.
        var i1: number, j1: number, k1: number; // Offsets for second corner of simplex in (i,j,k) coords
        var i2: number, j2: number, k2: number; // Offsets for third corner of simplex in (i,j,k) coords

        /* This code would benefit from a backport from the GLSL version! */
        if (x0 >= y0)
        {
            if (y0 >= z0)
            { i1 = 1; j1 = 0; k1 = 0; i2 = 1; j2 = 1; k2 = 0; } // X Y Z order
            else if (x0 >= z0) { i1 = 1; j1 = 0; k1 = 0; i2 = 1; j2 = 0; k2 = 1; } // X Z Y order
            else { i1 = 0; j1 = 0; k1 = 1; i2 = 1; j2 = 0; k2 = 1; } // Z X Y order
        }
        else
        { // x0<y0
            if (y0 < z0) { i1 = 0; j1 = 0; k1 = 1; i2 = 0; j2 = 1; k2 = 1; } // Z Y X order
            else if (x0 < z0) { i1 = 0; j1 = 1; k1 = 0; i2 = 0; j2 = 1; k2 = 1; } // Y Z X order
            else { i1 = 0; j1 = 1; k1 = 0; i2 = 1; j2 = 1; k2 = 0; } // Y X Z order
        }

        // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
        // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
        // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
        // c = 1/6.

        var x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
        var y1 = y0 - j1 + G3;
        var z1 = z0 - k1 + G3;
        var x2 = x0 - i2 + 2.0 * G3; // Offsets for third corner in (x,y,z) coords
        var y2 = y0 - j2 + 2.0 * G3;
        var z2 = z0 - k2 + 2.0 * G3;
        var x3 = x0 - 1.0 + 3.0 * G3; // Offsets for last corner in (x,y,z) coords
        var y3 = y0 - 1.0 + 3.0 * G3;
        var z3 = z0 - 1.0 + 3.0 * G3;

        // Wrap the integer indices at 256, to avoid indexing perm[] out of bounds
        var ii = Mod(i, 256);
        var jj = Mod(j, 256);
        var kk = Mod(k, 256);

        // Calculate the contribution from the four corners
        var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
        if (t0 < 0.0) n0 = 0.0;
        else
        {
            t0 *= t0;
            n0 = t0 * t0 * Grad3(_perm[ii + _perm[jj + _perm[kk]]], x0, y0, z0);
        }

        var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
        if (t1 < 0.0) n1 = 0.0;
        else
        {
            t1 *= t1;
            n1 = t1 * t1 * Grad3(_perm[ii + i1 + _perm[jj + j1 + _perm[kk + k1]]], x1, y1, z1);
        }

        var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
        if (t2 < 0.0) n2 = 0.0;
        else
        {
            t2 *= t2;
            n2 = t2 * t2 * Grad3(_perm[ii + i2 + _perm[jj + j2 + _perm[kk + k2]]], x2, y2, z2);
        }

        var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
        if (t3 < 0.0) n3 = 0.0;
        else
        {
            t3 *= t3;
            n3 = t3 * t3 * Grad3(_perm[ii + 1 + _perm[jj + 1 + _perm[kk + 1]]], x3, y3, z3);
        }

        // Add contributions from each corner to get the final noise value.
        // The result is scaled to stay just inside [-1,1]
        return 32.0 * (n0 + n1 + n2 + n3); // TODO: The scale factor is preliminary!
    }

    var PermOriginal = [
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180,
        151, 160, 137, 91, 90, 15,
        131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
        190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33,
        88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
        77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244,
        102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196,
        135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123,
        5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42,
        223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9,
        129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228,
        251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107,
        49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
        138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
    ];

    var _perm = PermOriginal.concat();

    function FastFloor(x: number)
    {
        return (x > 0) ? Math.floor(x) : (Math.floor(x) - 1);
    }

    function Mod(x: number, m: number)
    {
        var a = x % m;
        return a < 0 ? a + m : a;
    }

    function Grad1(hash: number, x: number)
    {
        var h = hash & 15;
        var grad = 1.0 + (h & 7);   // Gradient value 1.0, 2.0, ..., 8.0
        if ((h & 8) != 0) grad = -grad;         // Set a random sign for the gradient
        return (grad * x);           // Multiply the gradient with the distance
    }

    function Grad2(hash: number, x: number, y: number)
    {
        var h = hash & 7;      // Convert low 3 bits of hash code
        var u = h < 4 ? x : y;  // into 8 simple gradient directions,
        var v = h < 4 ? y : x;  // and compute the dot product with (x,y).
        return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0 * v : 2.0 * v);
    }

    function Grad3(hash: number, x: number, y: number, z: number)
    {
        var h = hash & 15;     // Convert low 4 bits of hash code into 12 simple
        var u = h < 8 ? x : y; // gradient directions, and compute dot product.
        var v = h < 4 ? y : h == 12 || h == 14 ? x : z; // Fix repeats at h = 12 to 15
        return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
    }

    function Grad4(hash: number, x: number, y: number, z: number, t: number)
    {
        var h = hash & 31;      // Convert low 5 bits of hash code into 32 simple
        var u = h < 24 ? x : y; // gradient directions, and compute dot product.
        var v = h < 16 ? y : z;
        var w = h < 8 ? z : t;
        return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v) + ((h & 4) != 0 ? -w : w);
    }
}