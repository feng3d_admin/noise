#### 参考
1. https://mrl.nyu.edu/~perlin/noise/
1. https://github.com/mrdoob/three.js/blob/dev/examples/js/math/SimplexNoise.js
1. http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
1. https://github.com/WardBenjamin/SimplexNoise
1. https://github.com/sarveshsvaran/Procedural-Volumetric-Particles-from-3d-4d-noise/blob/master/Assets/Noise.cs