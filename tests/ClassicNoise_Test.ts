(() =>
{

    var noise = new feng3d.Noise();


    var NUM = 100;

    console.log(`test ClassicNoise.perlin1 ClassicNoise.perlinN`);

    var errorNum = 0;
    for (let i = 0; i < NUM; i++)
    {
        var r = Math.random();
        var v0 = noise.perlin1(r);
        var v1 = noise.perlinN(r);

        if (v0 != v1)
        {
            errorNum++;
        }
    }
    console.log(`ClassicNoise.perlinN 实现 ClassicNoise.perlin1 ${errorNum == 0 ? "成功" : "错误"}！`)


    console.log(`test ClassicNoise.perlin2 ClassicNoise.perlinN`);
    var errorNum = 0;
    for (let i = 0; i < NUM; i++)
    {
        var r0 = Math.random();
        var r1 = Math.random();
        var v0 = noise.perlin2(r0, r1);
        var v1 = noise.perlinN(r0, r1);

        if (v0 != v1)
        {
            errorNum++;
        }
    }
    console.log(`ClassicNoise.perlinN 实现 ClassicNoise.perlin2 ${errorNum == 0 ? "成功" : "错误"}！`)

    console.log(`test ClassicNoise.perlin3 ClassicNoise.perlinN`);
    var errorNum = 0;
    for (let i = 0; i < NUM; i++)
    {
        var r0 = Math.random();
        var r1 = Math.random();
        var r2 = Math.random();
        var v0 = noise.perlin3(r0, r1, r2);
        var v1 = noise.perlinN(r0, r1, r2);

        if (v0 != v1)
        {
            errorNum++;
        }
    }
    console.log(`ClassicNoise.perlinN 实现 ClassicNoise.perlin3 ${errorNum == 0 ? "成功" : "错误"}！`)


    NUM = 1000000;

    console.log(`test ClassicNoise.perlin3 ClassicNoise.perlinN 性能`);

    var parms: number[][] = [];

    for (let i = 0; i < NUM; i++)
    {
        var r0 = Math.random();
        var r1 = Math.random();
        var r2 = Math.random();

        parms.push([r0, r1, r2]);
    }

    var t1 = Date.now();
    for (let i = 0; i < NUM; i++)
    {
        var v0 = noise.perlin3(parms[i][0], parms[i][1], parms[i][2]);
    }
    t1 = Date.now() - t1;

    var t2 = Date.now();
    for (let i = 0; i < NUM; i++)
    {
        var v0 = noise.perlinN(parms[i][0], parms[i][1], parms[i][2]);
    }
    t2 = Date.now() - t2;


    console.log(`ClassicNoise.perlinN(${t2}) / ClassicNoise.perlin3(${t1}) ${t2 / t1}！`)




})();